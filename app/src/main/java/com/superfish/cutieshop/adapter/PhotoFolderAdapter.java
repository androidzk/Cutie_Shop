package com.superfish.cutieshop.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.nostra13.universalimageloader.core.ImageLoader;
import com.superfish.cutieshop.BaseObjectListAdapter;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.been.PhotoBean;
import com.superfish.cutieshop.utils.ImageConfig;
import com.superfish.cutieshop.utils.ViewHolder;

import java.util.List;

public class PhotoFolderAdapter extends BaseObjectListAdapter {
    private int whichFolder;

    public PhotoFolderAdapter(Context context, List<? extends Object> datas) {
        super(context, datas);
    }

    // 传递当前选择的folder编号
    public void setWhichFolder(int whichFolder) {
        this.whichFolder = whichFolder;
    }

    public int getWhichFolder() {
        return whichFolder;
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        PhotoBean photoBean = (PhotoBean) mDatas.get(position);
        convertView = getConvertView(convertView, R.layout.item_photo_folder);
        ImageView folder_iv = ViewHolder.get(convertView, R.id.folder_iv);
        TextView folder_name = (TextView) convertView
                .findViewById(R.id.folder_name);
        TextView photo_count = (TextView) convertView
                .findViewById(R.id.photo_count);
        ImageView choose = (ImageView) convertView.findViewById(R.id.choose);

        ImageLoader.getInstance().displayImage(photoBean.getFirstImgPath(),
                folder_iv, ImageConfig.getPhotoOptions());
        String folderPath = photoBean.getDirPath();
        String folderName = "";
        if (folderPath != null) {
            int lastIndexOf = folderPath.lastIndexOf("/");
            folderName = folderPath.substring(lastIndexOf);
        }
        folder_name.setText(folderName.replace("/", ""));
        int photo_sum = photoBean.imagePaths.size() - 1;
        photo_count.setText("(" + photo_sum + ")");
        boolean isChoose = (getWhichFolder() == position);
        choose.setVisibility(isChoose ? View.VISIBLE : View.GONE);
        return convertView;
    }

}

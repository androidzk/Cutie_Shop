package com.superfish.cutieshop.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.superfish.cutieshop.BaseObjectListAdapter;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.utils.ImageConfig;
import com.superfish.cutieshop.utils.ViewHolder;

import java.util.List;


public class PhotoAdapter extends BaseObjectListAdapter {

    public PhotoAdapter(Context context, List<? extends Object> datas) {
        super(context, datas);
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        String photoPath = (String) mDatas.get(position);
        convertView = getConvertView(convertView, R.layout.item_photo);
        final ImageView camera_iv = ViewHolder.get(convertView, R.id.camera_iv);
        final ImageView camera_bg = ViewHolder.get(convertView, R.id.camera_bg);
        final ImageView photo_iv = ViewHolder.get(convertView, R.id.photo_iv);
        if (position == 0) {
            camera_iv.setVisibility(View.VISIBLE);
            camera_bg.setVisibility(View.VISIBLE);
            photo_iv.setVisibility(View.GONE);
        } else {
            camera_iv.setVisibility(View.GONE);
            camera_bg.setVisibility(View.GONE);
            photo_iv.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(photoPath, photo_iv,
                    ImageConfig.getPhotoOptions());
        }
        return convertView;
    }

}

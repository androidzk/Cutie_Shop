package com.superfish.cutieshop.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.superfish.cutieshop.BaseObjectListAdapter;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.been.Phase;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.ViewHolder;

import java.util.List;

/**
 * @author Jack
 */
public class PhaseItemAdapter extends BaseObjectListAdapter {
    public PhaseItemAdapter(Context context, List<? extends Object> datas) {
        super(context, datas);
        LogTest.wlj("数据集合大小=" + datas.size());
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        convertView = getConvertView(convertView, R.layout.gridview_item_emoticon);
        Phase item = (Phase) mDatas.get(position);
        TextView tv = ViewHolder.get(convertView, R.id.tv_emoticon);
        tv.setText(item.text);
        return convertView;
    }
}

package com.superfish.cutieshop.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.superfish.cutieshop.R;
import com.superfish.cutieshop.activities.MainActivity;
import com.superfish.cutieshop.utils.ViewHolder;

import java.util.List;

/**
 * @author Jack
 */
public class GuideAdapter extends BasePagerAdapter {
    public GuideAdapter(Context context, List<? extends Object> datas) {
        super(context, datas);
    }

    @Override
    public View getItemView(ViewGroup container, int position) {
        View view = View.inflate(mContext, R.layout.viewpager_item_guide, null);
        Integer res = (Integer) mDatas.get(position);
        ImageView iv = ViewHolder.get(view, R.id.guideIv);
        TextView startBtn = ViewHolder.get(view, R.id.startBtn);
        startBtn.setVisibility(position == 3 ? View.VISIBLE : View.GONE);
        iv.setImageResource(res);
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.open((Activity) mContext);
                ((Activity) mContext).finish();
            }
        });
        return view;
    }
}

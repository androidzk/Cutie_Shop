package com.superfish.cutieshop.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.superfish.cutieshop.BaseObjectListAdapter;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.activities.PhaseDetailActivity;
import com.superfish.cutieshop.been.ShopItem;
import com.superfish.cutieshop.utils.FontUtils;
import com.superfish.cutieshop.utils.ImageConfig;
import com.superfish.cutieshop.utils.TimeUtils;
import com.superfish.cutieshop.utils.ViewHolder;

import org.w3c.dom.Text;

import java.util.List;

/**
 * @author 大熊
 */
public class MainAdapter extends BaseObjectListAdapter {

    public MainAdapter(Context context, List<? extends Object> datas) {
        super(context, datas);
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        convertView = getConvertView(convertView, R.layout.listview_item_new_main);
        final ShopItem item = (ShopItem) mDatas.get(position);
        View mDaycontainer = ViewHolder.get(convertView, R.id.dayContainer);
        View zw = ViewHolder.get(convertView, R.id.zw);
        View zw1 = ViewHolder.get(convertView, R.id.zw1);
        View zw3 = ViewHolder.get(convertView, R.id.zw3);
        View headContainer = ViewHolder.get(convertView, R.id.headContainer);
        RelativeLayout dayBg = ViewHolder.get(convertView, R.id.dayBg);
        TextView mTitle = ViewHolder.get(convertView, R.id.title);
        TextView mSubTitle = ViewHolder.get(convertView, R.id.subTitle);
        ImageView mImage = ViewHolder.get(convertView, R.id.imageIv);
        TextView mWeeks = ViewHolder.get(convertView, R.id.weeks);
        TextView mDayTv = ViewHolder.get(convertView, R.id.day);
        TextView mMonthTv = ViewHolder.get(convertView, R.id.month);
        TextView mCompany = ViewHolder.get(convertView, R.id.company);
        TextView mCountTv = ViewHolder.get(convertView, R.id.count);
        ImageView mSignTv = ViewHolder.get(convertView, R.id.new_sign);
        LinearLayout mMainItembg = ViewHolder.get(convertView, R.id.mainItem);
        mTitle.setText(item.title);
        mSubTitle.setText(item.subtitle);
        dayBg.setBackgroundResource(TimeUtils.getIcons(item.online_time * 1000));
        ImageLoader.getInstance().displayImage(item.cover, mImage, ImageConfig.getMainOptions2());
        mWeeks.setText(TimeUtils.getWeekDay(item.online_time * 1000));
        mCompany.setTextColor(mContext.getResources().getColor(TimeUtils.bTheme(item.online_time * 1000)));
        mCompany.setText("  @" + TimeUtils.getJpWeekDay(item.online_time * 1000));
        mWeeks.setTextColor(mContext.getResources().getColor(TimeUtils.bTheme(item.online_time * 1000)));
        mCountTv.setBackgroundResource(TimeUtils.getCounts(item.online_time * 1000));
        headContainer.setBackgroundResource(TimeUtils.getHeads(item.online_time * 1000));
        zw3.setBackgroundColor(mContext.getResources().getColor(TimeUtils.cTheme(item.online_time * 1000)));
        String[] times = TimeUtils.splitTime(item.online_time * 1000);
        mDayTv.setText(times[2]);
        mDayTv.setTypeface(FontUtils.getTypefaceWithCode(mContext, 1));
        mMonthTv.setText(TimeUtils.getMonth(item.online_time * 1000) + "月");
        mCountTv.setText(item.counts + "");
        if (item.sign.equals("new")) {
            mSignTv.setVisibility(View.VISIBLE);
        } else {
            mSignTv.setVisibility(View.GONE);
        }
        mMainItembg.setBackgroundColor(mContext.getResources().getColor(TimeUtils.cTheme(item.online_time * 1000)));
        mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhaseDetailActivity.open((Activity) mContext, item.tid + "", item.title, item.subtitle, item.cover);
            }
        });
        if (position == 0) {
            //第一个显示出来
            mDaycontainer.setVisibility(View.VISIBLE);
            zw.setVisibility(View.VISIBLE);
            zw1.setVisibility(View.GONE);
        } else {
            ShopItem pre = (ShopItem) mDatas.get(position - 1);//
            if (TimeUtils.isSame(pre.online_time, item.online_time)) {
                //时间相同就隐藏
                mDaycontainer.setVisibility(View.GONE);
                zw.setVisibility(View.GONE);
                zw1.setVisibility(View.GONE);
            } else {
                mDaycontainer.setVisibility(View.VISIBLE);
                zw.setVisibility(View.VISIBLE);
                zw1.setVisibility(View.VISIBLE);
            }
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        return convertView;
    }
}

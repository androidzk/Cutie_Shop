package com.superfish.cutieshop.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.superfish.cutieshop.BaseApp;
import com.superfish.cutieshop.BaseObjectListAdapter;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.been.Product;
import com.superfish.cutieshop.dialog.AppDialog;
import com.superfish.cutieshop.dialog.BaseDialog;
import com.superfish.cutieshop.http.HttpStores;
import com.superfish.cutieshop.http.ZokeParams;
import com.superfish.cutieshop.parser.GsonFavorite;
import com.superfish.cutieshop.utils.CommonUtil;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.FontUtils;
import com.superfish.cutieshop.utils.HttpParams;
import com.superfish.cutieshop.utils.ImageConfig;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.TaobaoUtils;
import com.superfish.cutieshop.utils.ViewHolder;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @author JackWu
 */
public class FavoriteAdapter extends BaseObjectListAdapter {
    public FavoriteAdapter(Context context, List<? extends Object> datas) {
        super(context, datas);
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        convertView = getConvertView(convertView, R.layout.listview_item_fav);
        final Product pd = (Product) mDatas.get(position);
        TextView mPriceTv = ViewHolder.get(convertView, R.id.price);
        TextView mPriceTagTv = ViewHolder.get(convertView, R.id.priceTag);
        TextView mPrePrice = ViewHolder.get(convertView, R.id.pre_price);
        ImageView mImage = ViewHolder.get(convertView, R.id.image);
        TextView title = ViewHolder.get(convertView, R.id.title);
        TextView time = ViewHolder.get(convertView, R.id.time);

        ImageLoader.getInstance().displayImage(pd.thumb_detail, mImage, ImageConfig.getMainOptions());
        mPriceTv.setText(pd.discount_price + "");
        //设置字体
        mPriceTv.setTypeface(FontUtils.getTypefaceWithCode(mContext, 1));
        mPriceTagTv.setTypeface(FontUtils.getTypefaceWithCode(mContext, 1));
        //设置价格中划线
        mPrePrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        mPrePrice.setText("￥" + pd.price);
        title.setText(pd.title);
        //格式化
        SimpleDateFormat format = new SimpleDateFormat("MM-dd HH:mm");
        time.setText(format.format(pd.create_time * 1000));
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转到淘宝商店 0淘宝 1天猫  客户端是 1淘宝 2天猫
                TaobaoUtils.showTaokeItemDetail((Activity) mContext, pd.shop_type + 1, pd.open_iid);
            }
        });
        final int pos = position;
        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //弹出框 删除
                final AppDialog mDialog = new AppDialog(mContext);
                mDialog.setDialogTitle("取消收藏");
                mDialog.setSubTitle("取消收藏将移出心愿清单");
                mDialog.setLeftText("取消");
                mDialog.setRightText("确定");
                mDialog.show();
                mDialog.setLeftClick(new BaseDialog.LeftClickLinstener() {
                    @Override
                    public void onClickLeft(View view) {
                        mDialog.dismiss();
                    }
                });
                mDialog.setRightClick(new BaseDialog.RightClickLinstener() {
                    @Override
                    public void onClickRight(View view) {
                        //取消收藏
                        mDialog.dismiss();
                        if (!CommonUtil.getNetWorkStates(mContext)) {
                            CutieToast.showNetFailed(mContext);
                            return;
                        }
                        HttpUtils mHttp = HttpStores.getInstense().getHttp();
                        final ZokeParams params = HttpParams.addDetail(-1, 1, pd.iid);
                        params.setHeader("token", BaseApp.getNextUidToken());
                        mHttp.send(HttpRequest.HttpMethod.POST, params.getUrl(), params, new RequestCallBack<String>() {
                            @Override
                            public void onSuccess(ResponseInfo<String> responseInfo) {
                                params.setResult(responseInfo);
                                String result = responseInfo.result;
                                LogTest.wlj("result=" + result);
                                GsonFavorite gf = new GsonFavorite().fromJson(result);
                                if (gf.info == null || gf.error_code != 0) {
                                    CutieToast.showNetFailed(mContext);
                                    return;
                                }
                                mDatas.remove(pos);
                                notifyDataSetChanged();
                                CutieToast.show(mContext, "取消成功");
                            }

                            @Override
                            public void onFailure(HttpException e, String s) {
                                CutieToast.showNetFailed(mContext);
                                //回退状态
                            }
                        });
                    }
                });
                return true;
            }
        });
        return convertView;
    }
}

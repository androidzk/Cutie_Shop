package com.superfish.cutieshop.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.BaseObjectListAdapter;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.been.Notify;
import com.superfish.cutieshop.utils.ImageConfig;
import com.superfish.cutieshop.utils.ViewHolder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by lmh on 15/9/15.
 */
public class NotifyAdapter extends BaseObjectListAdapter {
    private SimpleDateFormat sdf;
    public NotifyAdapter(Context context, List<? extends Object> datas) {
        super(context, datas);
        sdf = new SimpleDateFormat("yyyy.MM.dd");
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        convertView = getConvertView(convertView, R.layout.listview_item_notify);
        ImageView iv_point = ViewHolder.get(convertView, R.id.iv_point);
        ImageView iv_avatar = ViewHolder.get(convertView, R.id.iv_avatar);
        TextView tv_title = ViewHolder.get(convertView, R.id.tv_title);
        TextView tv_content = ViewHolder.get(convertView, R.id.tv_content);
        TextView tv_date = ViewHolder.get(convertView, R.id.tv_date);
        Notify notify = (Notify) getItem(position);
        tv_title.setText("系统通知");
        tv_content.setText(notify.content);
        tv_date.setText(sdf.format(new Date(notify.time * 1000)));
        ImageLoader.getInstance().displayImage(notify.avatar, iv_avatar, ImageConfig.getCirclHeadOptions());
        if (notify.read == BaseConfig.NOTIFICATION_READ) {
            iv_point.setVisibility(View.GONE);
        } else {
            iv_point.setVisibility(View.VISIBLE);
        }
        return convertView;
    }
}

package com.superfish.cutieshop.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.superfish.cutieshop.BaseObjectListAdapter;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.ViewHolder;

import java.util.List;

/**
 * Created by lmh on 15/10/13.
 */
public class RadioAdapter extends BaseObjectListAdapter {
    public RadioAdapter(Context context, List<? extends Object> datas) {
        super(context, datas);
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        convertView = getConvertView(convertView, R.layout.listview_item_radiodialog);
        TextView tv_text = ViewHolder.get(convertView, R.id.tv_text);
        String str = (String) mDatas.get(position);
        LogTest.lmh("getItemView=" + str);
        tv_text.setText(str);
        return convertView;
    }
}

package com.superfish.cutieshop.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.superfish.cutieshop.BaseObjectListAdapter;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.been.CommentInfo;
import com.superfish.cutieshop.utils.ImageConfig;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.TimeUtils;
import com.superfish.cutieshop.utils.ViewHolder;

import java.util.List;

/**
 * 聊天适配器
 *
 * @author JackWu
 */
public class ChatAdapter extends BaseObjectListAdapter {
    public ChatAdapter(Context context, List<? extends Object> datas) {
        super(context, datas);
    }


//    @Override
//    public int getItemViewType(int position) {
//        CommentInfo info = (CommentInfo) mDatas.get(position);
//        return info.msgType;
//    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        CommentInfo cm = (CommentInfo) mDatas.get(position);
        if (cm.msgType == 1) {
            //发送
            convertView = getConvertView(convertView, R.layout.listview_item_comment_right);
        } else if (cm.msgType == 2) {
            //接收
            convertView = getConvertView(convertView, R.layout.listview_item_comment_left);
        } else {
            convertView = getConvertView(convertView, R.layout.listview_item_comment_right);
        }
        // 查找控件 设置数据
        ImageView mHeadIv = ViewHolder.get(convertView, R.id.headTv);
        TextView mNameTv = ViewHolder.get(convertView, R.id.name);
        TextView mTimeTv = ViewHolder.get(convertView, R.id.time);
        TextView mContentTv = ViewHolder.get(convertView, R.id.content);
        ImageLoader.getInstance().displayImage(cm.user_info.avatar, mHeadIv, ImageConfig.getCirclHeadOptions());
        mNameTv.setText(cm.user_info.nikename);
        mTimeTv.setText(TimeUtils.getTimeStr(cm.comment_info.create_time));
        mContentTv.setText(cm.comment_info.text);
        return convertView;
    }
}

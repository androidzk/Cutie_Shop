package com.superfish.cutieshop.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.superfish.cutieshop.R;

import java.util.List;

public class GuidePointIndicatorAdapter extends BaseIndicatorAdapter {

    public GuidePointIndicatorAdapter(List<? extends Object> list,
                                      Context context) {
        super(list, context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = getConvertView(convertView,
                R.layout.guide_indicator_item_point);
        return convertView;
    }

}

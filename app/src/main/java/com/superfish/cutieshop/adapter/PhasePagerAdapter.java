package com.superfish.cutieshop.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.superfish.cutieshop.BaseApp;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.activities.LoginActivity;
import com.superfish.cutieshop.activities.StrategyDetailActivity;
import com.superfish.cutieshop.been.PhaseDetail;
import com.superfish.cutieshop.been.ShareContent;
import com.superfish.cutieshop.dialog.ShareDialog;
import com.superfish.cutieshop.http.CallBackStores;
import com.superfish.cutieshop.http.HttpError;
import com.superfish.cutieshop.http.HttpStores;
import com.superfish.cutieshop.http.ZokeParams;
import com.superfish.cutieshop.parser.GsonFavorite;
import com.superfish.cutieshop.utils.CommonUtil;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.FontUtils;
import com.superfish.cutieshop.utils.HttpParams;
import com.superfish.cutieshop.utils.ImageConfig;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.ViewHolder;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

/**
 * 稿件详情列表
 */
public class PhasePagerAdapter extends BasePagerAdapter {
    private ShareDialog shareDialog;

    private String tid;//统计使用

    public PhasePagerAdapter(Context context, List<? extends Object> datas) {
        super(context, datas);
        shareDialog = new ShareDialog(mContext);
    }

    public PhasePagerAdapter(Context context, List<? extends Object> datas, String tid) {
        super(context, datas);
        shareDialog = new ShareDialog(mContext);
        this.tid = tid;
    }

    @Override
    public View getItemView(ViewGroup container, int position) {
        View view = mInflater.inflate(R.layout.vp_item_phase, null);
        final PhaseDetail pd = (PhaseDetail) mDatas.get(position);
        TextView title = ViewHolder.get(view, R.id.title);
        TextView auth = ViewHolder.get(view, R.id.author);
        ImageView image = ViewHolder.get(view, R.id.image);
        TextView price = ViewHolder.get(view, R.id.price);
        TextView discibe = ViewHolder.get(view, R.id.discribe);
        TextView priceTag = ViewHolder.get(view, R.id.priceTag);
        final ImageView likeIv = ViewHolder.get(view, R.id.likeBtn);
        View shareBtn = ViewHolder.get(view, R.id.shareBtn);
        title.setText(pd.title);
        auth.setText("by " + pd.author);
        ImageLoader.getInstance().displayImage(pd.thumb, image, ImageConfig.getMainOptions());
        price.setText(pd.discount_price + "");
        price.setTypeface(FontUtils.getTypefaceWithCode(mContext, 1));
        priceTag.setTypeface(FontUtils.getTypefaceWithCode(mContext, 1));
        discibe.setText(pd.profile);
        likeIv.setImageResource(pd.detail == 1 ? R.drawable.btn_like : R.drawable.btn_unlike_);
        if (TextUtils.isEmpty(pd.open_iid)) {
            priceTag.setVisibility(View.GONE);
            price.setVisibility(View.GONE);
            likeIv.setVisibility(View.GONE);
        } else {
            priceTag.setVisibility(View.VISIBLE);
            price.setVisibility(View.VISIBLE);
            likeIv.setVisibility(View.VISIBLE);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StrategyDetailActivity.open((Activity) mContext, pd.title, pd.share_url, pd.iid, pd.detail_counts, pd.detail == 1, pd.url, !TextUtils.isEmpty(pd.open_iid), pd.thumb, pd.profile);
            }
        });
        likeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(mContext, BaseConfig.UM.GM_FAV_PHASE);
                if (BaseApp.isGuest()) {
                    LoginActivity.open((Activity) mContext);
                    return;
                }

                if (!CommonUtil.getNetWorkStates(mContext)) {
                    CutieToast.showNetFailed(mContext);
                    return;
                }
                // 提前状态设置
                //收藏
                likeIv.setImageResource(pd.detail == 0 ? R.drawable.btn_like : R.drawable.btn_unlike_);
                HttpUtils mHttp = HttpStores.getInstense().getHttp();
                final ZokeParams params = HttpParams.addDetail(-1, pd.detail, pd.iid);
                params.setHeader("token", BaseApp.getNextUidToken());
                mHttp.send(HttpRequest.HttpMethod.POST, params.getUrl(), params, new RequestCallBack<String>() {
                    @Override
                    public void onSuccess(ResponseInfo<String> responseInfo) {
                        params.setResult(responseInfo);
                        String result = responseInfo.result;
                        LogTest.wlj("result=" + result);
                        GsonFavorite gf = new GsonFavorite().fromJson(result);
                        if (gf.info == null || gf.error_code != 0) {
                            likeIv.setImageResource(pd.detail == 1 ? R.drawable.btn_like : R.drawable.btn_unlike_);
                            return;
                        }
                        //  status 0没有，1有
                        pd.detail = gf.info.status;
                        likeIv.setImageResource(pd.detail == 1 ? R.drawable.btn_like : R.drawable.btn_unlike_);
                    }

                    @Override
                    public void onFailure(HttpException e, String s) {
                        LogTest.wlj(e.getExceptionCode() + "");
                        CutieToast.show(mContext, "操作失败");
                        //回退状态
                        likeIv.setImageResource(pd.detail == 1 ? R.drawable.btn_like : R.drawable.btn_unlike_);
                    }
                });
            }
        });
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(mContext, BaseConfig.UM.GM_SHARE_PHASE);
                //分享操作
                ShareContent sc = ShareContent.getPhaseShareContent(pd.iid, pd.title, pd.share_url, pd.thumb, pd.profile);
                shareDialog.setShareContent(sc);
                shareDialog.show();
            }
        });
        return view;
    }

}

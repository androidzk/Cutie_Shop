package com.superfish.cutieshop.adapter;

import java.util.List;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.superfish.cutieshop.R;

public class ChatEmoticonPointIndicatorAdapter extends BaseIndicatorAdapter {

    public ChatEmoticonPointIndicatorAdapter(List<? extends Object> list,
                                             Context context) {
        super(list, context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = getConvertView(convertView,
                R.layout.chat_emoticon_indicator_item_point);
        return convertView;
    }

}

package com.superfish.cutieshop.been;

public class PhaseDetail {
    public int iid;
    public String title;
    public String author;
    public String thumb;
    public String price;
    public String open_iid;//没有openId的是不会显示价格的
    public String profile;
    public long online_time;
    public String discount_price;//原价
    public String url;
    public String share_url;
    public int detail;//是否在个人清单内,是否喜欢
    public int detail_counts;//有多少人添加了心愿清单

}

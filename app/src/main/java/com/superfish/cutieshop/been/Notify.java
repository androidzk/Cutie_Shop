package com.superfish.cutieshop.been;

import com.lidroid.xutils.db.annotation.Table;

import java.io.Serializable;

/**
 * Created by lmh on 15/9/15.
 */
@Table(name = "cutie_notify")
public class Notify implements Serializable{
    private int id;
    public String avatar;
    public String content;
    public long time;
    public String value;
    public int type;
    public int read;
}

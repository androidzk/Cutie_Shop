package com.superfish.cutieshop.been;

import java.io.Serializable;

/**
 * 首页信息
 *
 * @author Jack
 */
public class ShopItem implements Serializable{
    public int tid;
    public String title;
    public String subtitle;
    public long online_time;
    public String cover;
    public int counts;
    public String sign;
}

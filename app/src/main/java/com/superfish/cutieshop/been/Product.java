package com.superfish.cutieshop.been;

/**
 * 商品
 */
public class Product {
    public int id;
    public String open_iid;
    public long create_time;
    public String thumb_detail;
    public String title;
    public double price;
    public double discount_price;
    public int shop_type;
    public int iid;
}

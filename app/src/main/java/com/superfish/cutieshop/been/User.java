package com.superfish.cutieshop.been;

import com.lidroid.xutils.db.annotation.Table;

import java.io.Serializable;

@Table(name = "cutie_user")
public class User implements Serializable {
	// 用作用户表中的唯一标识
	public int id;//
	public int uid;// 用户uid
	public String avatar;
	public int gender;
	public String nikename;
	public int isThird;		// 0-不是, 1-是
}

package com.superfish.cutieshop.been;

import com.superfish.cutieshop.BaseConfig;

import java.io.Serializable;

/**
 * 分享内容的实体类
 *
 * @author lmh
 */
public class ShareContent implements Serializable {
    private String text;            //分享文字
    private String url;                //分享链接
    private String imgUrl;            //图片链接
    private String title;               //标题
    private String site;                //site

    public int getIid() {
        return iid;
    }

    public void setIid(int iid) {
        this.iid = iid;
    }

    private int iid;                //从哪里分享

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    /**
     * 按照设定好的文字返回内容，若文字内容未空，则返回预设文字
     *
     * @return
     */
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 按照设定好的图片返回内容，若图片为空，则返回预设图片
     *
     * @return
     */
    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public ShareContent(int iid) {
        super();
        this.iid = iid;
    }

    public static ShareContent getTestShareContent(int iid) {
        ShareContent sc = new ShareContent(iid);
        sc.setText(BaseConfig.SHARE_TEXT);
        sc.setImgUrl(BaseConfig.SHARE_IMAGE);
        sc.setUrl(BaseConfig.SHARE_URL);
        return sc;
    }

    public static ShareContent getMainShareContent() {
        ShareContent sc = new ShareContent(0);
        sc.setText(BaseConfig.SHARE_TEXT);
        sc.setImgUrl(BaseConfig.SHARE_IMAGE);
        sc.setUrl(BaseConfig.SHARE_URL);
        sc.setTitle(BaseConfig.SHARE_TITLE);
        sc.setSite(BaseConfig.SHARE_SITE);
        sc.setUrl(BaseConfig.SHARE_URL);
        return sc;
    }

    public static ShareContent getPhaseShareContent(int iid, String title, String shareUrl, String imageUrl, String dec) {
        ShareContent shareContent = new ShareContent(iid);
        shareContent.setTitle(title);
        shareContent.setImgUrl(imageUrl);
        shareContent.setUrl(shareUrl);
        shareContent.setSite(BaseConfig.SHARE_SITE);
        shareContent.setText(dec);
        return shareContent;
    }

}

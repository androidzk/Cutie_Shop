package com.superfish.cutieshop.been;

import java.util.LinkedList;
/**
 * 系统相册数据
 * @author luxin
 *
 */
public class PhotoBean {
	private String dirPath;
	private String firstImgPath;
	public LinkedList<String> imagePaths = new LinkedList<String>();
    public PhotoBean(){
		// //相机
		// imagePaths.add("drawable://" + R.drawable.ic_launcher);
    }
	public String getDirPath() {
		
		return dirPath;
	}

	public void setDirPath(String dirPath) {
		this.dirPath = dirPath;
	}
	public String getFirstImgPath() {
		return firstImgPath;
	}

	public void setFirstImgPath(String firstImgPath) {
		this.firstImgPath = firstImgPath;
	}

	@Override
	public String toString() {
		return "PhotoBean [dirPath=" + dirPath + ", imagePaths=" + imagePaths
				+ "]";
	}

}

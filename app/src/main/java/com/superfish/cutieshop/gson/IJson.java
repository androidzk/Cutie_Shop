package com.superfish.cutieshop.gson;

import org.json.JSONObject;

/**
 */
public interface IJson<T> {
	String toJson();

	T fromJson(String json);

	boolean isBelongToMe(JSONObject json);
}

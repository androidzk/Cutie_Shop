package com.superfish.cutieshop.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * Json序列化和反序列化的Gson封装。静态变量会被忽略掉，不受影响。
 * 
 */
public abstract class AbsJson<T> implements IJson<T> {
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss:SSS";
	private static final GsonBuilder sGsonBuilder = new GsonBuilder()
			.excludeFieldsWithoutExposeAnnotation() // 不导出实体中没有用@Expose注解的属性
			.enableComplexMapKeySerialization() // 支持Map的key为复杂对象的形式
			.serializeNulls() // null值也进行输出
			.disableHtmlEscaping() // 取消unicode及等号的转义
			.setDateFormat(DATE_FORMAT) // 时间转化为特定格式
	// .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE) //会把字段首字母大写
	;

	public static <D> D fromJsonWithAllFields(String json, Class<D> clazz) {
		return new Gson().fromJson(json, clazz);
	}

	public static <D> D fromJsonWithAllFields(String json, TypeToken<D> type) {
		return new Gson().fromJson(json, type.getType());
	}

	public static String toJsonWithAllFields(Object o) {
		return new Gson().toJson(o);
	}

	public static <D> D fromJsonWithExposeAnnoFields(String json, Class<D> clazz) {
		return sGsonBuilder.create().fromJson(json, clazz);
	}

	public static <D> D fromJsonWithExposeAnnoFields(String json,
			TypeToken<D> type) {
		return sGsonBuilder.create().fromJson(json, type.getType());
	}

	public static String toJsonWithExposeAnnoFields(Object o) {
		return sGsonBuilder.create().toJson(o);
	}
}

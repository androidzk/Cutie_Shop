package com.superfish.cutieshop.gson;

import com.google.gson.annotations.Expose;

/**
 * @author Wei.Chou
 */
public abstract class AbsData<T extends AbsData<T>> extends AbsJsonTyped<T> {
    public static final String KEY_RESULT = "request";

    //共用部分抽离
    public String request;
    public String version;
    public int error_code;
    public String error;

    @Expose
    public String result;

    @Override
    protected String typeKey() {
        return KEY_RESULT;
    }
}

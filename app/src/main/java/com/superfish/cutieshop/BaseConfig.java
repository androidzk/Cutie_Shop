package com.superfish.cutieshop;

/**
 * 配置信息
 */
public class BaseConfig {
    public static final String APP_NAME = "购萌早报";
    public static final String SHARE_TEXT = "在\"购萌早报\"每天都能发现好玩的东西，快来看看吧。";
    public static final String SHARE_IMAGE = "http://static.ipicopico.com/func/35b108af750752a0af34aba07eb08500.jpg";
    public static final String SHARE_TITLE = "邀请你和我一起来玩\"购萌早报\"";
    public static final String SHARE_SITE = "购萌早报";
    public static final String SHARE_URL = "http://gomeng.ipicopico.com/?way=app";
    public static final String SHARE_CONTRIBUTION_TEXT = "我在\"购萌早报\"发现了好玩的东西，快来看看吧。";
    public static final String APP_DIR_NAME = "SuperFish";
    public static final String TAOBAOKE_PID = "mm_111480045_0_0";
    public static final int NOTIFICATION_ID_CONTRIBUTION = 2020;
    public static final int NOTIFICATION_ID_WEBPAGE = 2021;
    public static final int NOTIFICATION_READ = 1;
    public static final int NOTIFICATION_UNREAD = 0;
    public static final String BROADCAST_ACTION = "com.superfish.cutieshop.activities.MainActivity";            //广播action，发送给主页更新通知状态
    //用户协议
    public static final String AGREEMENT_URL = "http://gomeng.ipicopico.com/agreement";
    public static final String BUYER = "http://gomeng.ipicopico.com/buyer.html";

    public static final String SMS_APPKEY = "2f00a6daba2e";
    public static final String SMS_APPSECRET = "edc512c29040c2587300d791c4bb3107";
    public static String DEFAULT_COUNTRY_ID = "86";
    public static final int RETRY_INTERVAL = 60;

    public static final String INTENT_SHOPITEM = "intent_shopItem";

    public static final String WEIBO_APPKEY = "3363840295";
    public static final String WEIBO_REDIRECTURL = "https://api.weibo.com/oauth2/default.html";
    public static final String WEIBO_SCOPE = "follow_app_official_microblog";

    public static class ShareKey {
        public static final String TOKEN = "cutie_token";//token
        public static final String PHONE_DEVICE = "cutie_phoneDeviceId";//设备id
        public static final String NOTIFY_SWITCH = "cutie_switch";//通知开关 true--关 false 开
        public static final String VERSIONCHECKKEY = "cutie_version";//版本检测
        public static final String HAS_NEW_NOTIFY = "has_new_notify";
        public static final String SPLASH_KEY = "splash_key";//
        public static final String ISLOGIN = "cutie_isLogin";
    }


    /**
     * 链接
     **/
    public static class UrlBank {
        //开发环境
//        private static final String HTTPURL = "http://gmapitest2015.ipicopico.com/";
        //现网环境
        public static final String HTTPURL = "http://gmapi.ipicopico.com/";
        //设备号登陆
        public static final String login_device = HTTPURL + "v1/accounts/login";
        //获取主页列表
        public static final String mainList = HTTPURL + "v1/articles/timeline";
        //版本更新
        public static final String version = HTTPURL + "v1/tools/version";
        //弹幕列表
        public static final String barrages = HTTPURL + "v1/comments/list";
        //添加弹幕
        public static final String addBarrage = HTTPURL + "v1/comments/add";
        //快捷回复列表
        public static final String phases = HTTPURL + "v1/articles/phrases";
        //统计分享
        public static final String analysShare = HTTPURL + "v1/tools/share";
        //获取稿件详情
        public static final String info = HTTPURL + "v1/articles/info";
        //手机号进行注册
        public static final String mob_reg = HTTPURL + "v1/accounts/mob-reg";
        //手机号注册的时候判断是不是已经被注册了
        public static final String check_mobile = HTTPURL + "v1/accounts/check-mobile";
        //第三方账户登录
        public static final String tri_login = HTTPURL + "v1/accounts/tri-login";
        //手机号登录
        public static final String mob_login = HTTPURL + "v1/accounts/mob-login";
        //修改个人资料
        public static final String edit = HTTPURL + "v1/users/edit";
        //忘记密码
        public static final String forget_pwd = HTTPURL + "v1/accounts/forget-pwd";
        //修改密码
        public static final String edit_pwd = HTTPURL + "v1/accounts/edit-pwd";
        //首页数据 1.1
        public static final String mainList_v1 = HTTPURL + "v1/presses/press-list";
        //列表详情
        public static final String phaseDetail = HTTPURL + "v1/presses/item-list";
        //评论列表
        public static final String comments = HTTPURL + "v1/item-comments/list";
        //添加评论
        public static final String addComment = HTTPURL + "v1/item-comments/add";
        //删除评论
        public static final String delComment = HTTPURL + "v1/item-comments/del";
        //商品对外分享
        public static final String itemShare = HTTPURL + "v1/tools/item-share";
        //添加到心愿清单
        public static final String addDetail = HTTPURL + "v1/details/add";
        //从心愿清单中移除
        public static final String delDetail = HTTPURL + "v1/details/del";
        //清单列表
        public static final String details = HTTPURL + "v1/details/list";
    }

    /**
     * 友盟自定义事件统计
     **/
    public static class UM {
        public static final String CS_SHARETOQQ = "CS_SHARETOQQ";
        public static final String CS_SHARETOWEIBO = "CS_SHARETOWEIBO";
        public static final String CS_SHARETOQZONE = "CS_SHARETOQZONE";
        public static final String CS_SHARETOWECHAT = "CS_SHARETOWECHAT";
        public static final String CS_SHARETOWECHATMOMENTS = "CS_SHARETOWECHATMOMENTS";
        //友盟统计 1.1版本 自定义统计事件
        //稿件页点击收藏的次数
        public static final String GM_FAV_PHASE = "GM_FAV_PHASE";
        //稿件页点击分享的次数
        public static final String GM_SHARE_PHASE = "GM_SHARE_PHASE";
        //商品描述页点击收藏的次数
        public static final String GM_FAV_DES = "GM_FAV_DES";
        //商品描述页点击分享的次数
        public static final String GM_SHARE_DES = "GM_SHARE_DES";
        //商品描述页面点击评论的次数
        public static final String GM_COMMENT_DES = "GM_COMMENT_DES";

    }

    public static class MessageCode {
        public static final int ON_NEW_UMMESSAGE = 1;
        public static final int ON_READ_UMMESSAGE = 2;
        public static final int ON_SMS_CALLBACK = 3;
        public static final int CROPER = 4;
        public static final int EXIT = 5;
        public static final int ON_REGISTER_COMPLETE = 6;
        public static final int UPDATE_USER = 7;
        public static final int ON_RESETPASSWORD_COMPLETE = 8;
    }


}

package com.superfish.cutieshop.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.superfish.cutieshop.R;

public abstract class BaseDialog extends Dialog implements View.OnClickListener {

    protected TextView mTitleTv;
    protected TextView mContentTv;
    protected TextView mLeftBtn;
    protected TextView mRightBtn;
    protected LinearLayout mSubContainer;
    /**
     * 是否可以取消 默认为true
     **/
    protected boolean isCancelable = true;
    /**
     * 是否点击外面可以取消弹出框 默认为true
     **/
    protected boolean isCanceledOnOutside = true;

    protected LayoutInflater mInflater;

    private LeftClickLinstener mLeftLinstener;
    private RightClickLinstener mRightLinstener;

    public BaseDialog(Context context, boolean cancelable,
                      OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public BaseDialog(Context context, int theme) {
        super(context, R.style.BaseDialogStyle);
    }

    public BaseDialog(Context context) {
        super(context, R.style.BaseDialogStyle);
        mInflater = LayoutInflater.from(context);
        setContentView(getLayoutRes());
        windowDeploy(context);
        // dialog的标题
        mTitleTv = (TextView) findViewById(R.id._basedialog_title);
        //dialog的正文
        mContentTv = (TextView) findViewById(R.id._basedialog_content);
        // 左侧按钮
        mLeftBtn = (TextView) findViewById(R.id._basedialog_leftbutton);
        // 右侧按钮
        mRightBtn = (TextView) findViewById(R.id._basedialog_rightbutton);
        // 子控件的容器
        mSubContainer = (LinearLayout) findViewById(R.id._basedialog_subcontainer);
        mLeftBtn.setOnClickListener(this);
        mRightBtn.setOnClickListener(this);
        setCancelable(isCancelable);
        setCanceledOnTouchOutside(isCanceledOnOutside);
    }

    public BaseDialog setCancelabled(boolean isCancel) {
        setCancelable(isCancel);
        return this;
    }

    public BaseDialog setCancelOutside(boolean isCacel) {
        setCanceledOnTouchOutside(isCacel);
        return this;
    }

    public BaseDialog setContent(CharSequence text) {
        if (null != mContentTv)
            mContentTv.setText(text);
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * dialog的布局
     **/
    protected abstract int getLayoutRes();

    /**
     * 设置标题
     **/
    public BaseDialog setDialogTitle(CharSequence text) {
        if (null != mTitleTv)
            mTitleTv.setText(text);
        return this;
    }

    /**
     * 设置左侧按钮文本
     **/
    public BaseDialog setLeftText(CharSequence text) {
        if (null != mLeftBtn)
            mLeftBtn.setText(text);
        return this;
    }

    /**
     * 设置左侧点击事件
     **/
    public BaseDialog setLeftClick(LeftClickLinstener listener) {
        this.mLeftLinstener = listener;
        return this;
    }

    /**
     * 设置右侧按钮文本
     **/
    public BaseDialog setRightText(CharSequence text) {
        if (null != mRightBtn)
            mRightBtn.setText(text);
        return this;
    }

    /**
     * 设置右侧按钮点击监听
     **/
    public BaseDialog setRightClick(RightClickLinstener listener) {
        this.mRightLinstener = listener;
        return this;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id._basedialog_leftbutton) {
            onClickLeft(view);
            return;
        }

        if (id == R.id._basedialog_rightbutton) {
            onClickRight(view);
            return;
        }
    }

    /**
     * 左侧按钮点击事件
     **/
    private void onClickLeft(View view) {
        if (null != mLeftLinstener)
            mLeftLinstener.onClickLeft(view);
    }

    /**
     * 右侧按钮点击
     **/
    private void onClickRight(View view) {
        if (null != mRightLinstener)
            mRightLinstener.onClickRight(view);
    }

    /**
     * 左侧按钮点击监听
     **/
    public interface LeftClickLinstener {
        public void onClickLeft(View view);
    }

    /**
     * 右侧按钮点击监听
     **/
    public interface RightClickLinstener {
        public void onClickRight(View view);
    }

    /**
     * 设置副标题或者是其他控件
     **/
    public BaseDialog setSubContent(View view) {
        if (null != mSubContainer) {
            if (mSubContainer.getChildCount() > 0) {
                mSubContainer.removeAllViews();
            }
            mSubContainer.setVisibility(View.VISIBLE);
            RelativeLayout result = new RelativeLayout(getContext());
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            result.addView(view, params);
            // 添加控件
            mSubContainer.addView(result, new LinearLayout.LayoutParams(
                    android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                    android.view.ViewGroup.LayoutParams.MATCH_PARENT, 1));
        }
        return this;
    }

    public BaseDialog hideSubContent(View view) {
        if (null != mSubContainer)
            mSubContainer.setVisibility(View.GONE);
        return this;
    }

    private void windowDeploy(Context context) {
        WindowManager m = ((Activity) context).getWindowManager();
        Display d = m.getDefaultDisplay();
        Window window = getWindow(); //
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.width = d.getWidth();
        window.setAttributes(wl);
//        window.setGravity(whichGravity);
    }
}

package com.superfish.cutieshop.dialog;

import android.content.Context;
import android.view.View;

import com.superfish.cutieshop.R;

/**
 * Created by lmh on 15/10/14.
 */
public class GenderDialog extends BaseBottomDialog implements View.OnClickListener {
    private Context mContext;
    private OnGenderClickListener onGenderClickListener;

    public GenderDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.dialog_gender);
    }

    @Override
    protected void init() {
        findViewById(R.id.tv_male).setOnClickListener(this);
        findViewById(R.id.tv_female).setOnClickListener(this);
        findViewById(R.id.tv_cancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_male:
                onGenderClickListener.OnGenderClick(mContext.getResources().getString(R.string.register_gender_male));
                this.dismiss();
                break;
            case R.id.tv_female:
                onGenderClickListener.OnGenderClick(mContext.getResources().getString(R.string.register_gender_female));
                this.dismiss();
                break;
            case R.id.tv_cancel:
                this.dismiss();
                break;
            default:
                break;
        }
    }

    public interface OnGenderClickListener {
        public void OnGenderClick(String gender);
    }

    public void setOnGenderClickListener(OnGenderClickListener onGenderClickListener) {
        this.onGenderClickListener = onGenderClickListener;
    }
}

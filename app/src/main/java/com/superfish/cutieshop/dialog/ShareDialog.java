package com.superfish.cutieshop.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.exception.WeiboException;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.been.ShareContent;
import com.superfish.cutieshop.utils.AccessTokenKeeper;
import com.superfish.cutieshop.utils.CommonUtil;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.ShareUtils;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;

/**
 * Created by lmh on 15/9/15.
 */
public class ShareDialog extends BaseBottomDialog implements View.OnClickListener {
    private PlatformActionListener platformActionListener;
    private AppLoading appLoading;
    private ShareContent shareContent;
    private WeiboAuthListener weiboAuthListener;

    public ShareDialog(Context context) {
        super(context);
        appLoading = new AppLoading(context);
        appLoading.setTips(R.string.apploading_loading);
    }

    public ShareDialog(Context context, ShareContent sc) {
        super(context);
        appLoading = new AppLoading(context);
        appLoading.setTips(R.string.apploading_loading);
        shareContent = sc;
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.dialog_share);
    }

    @Override
    protected void init() {
        platformActionListener = new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        CutieToast.show(context, "onComplete");
                        appLoading.dismiss();
                    }
                });
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        CutieToast.show(context, "onError");
                        appLoading.dismiss();
                    }
                });
            }

            @Override
            public void onCancel(Platform platform, int i) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        CutieToast.show(context, "onCancel");
                        appLoading.dismiss();
                    }
                });
            }
        };
        weiboAuthListener = new WeiboAuthListener() {
            @Override
            public void onWeiboException(WeiboException arg0) {
                appLoading.dismiss();
                CutieToast.show(context, context.getResources().getString(R.string.global_error_accident));
            }

            @Override
            public void onComplete(Bundle bundle) {
                Oauth2AccessToken newToken = Oauth2AccessToken.parseAccessToken(bundle);
                AccessTokenKeeper.writeAccessToken(context.getApplicationContext(), newToken);
                appLoading.dismiss();
            }

            @Override
            public void onCancel() {
                appLoading.dismiss();
                CutieToast.show(context, "微博分享取消");
            }
        };
        findViewById(R.id.iv_sharewechat).setOnClickListener(this);
        findViewById(R.id.iv_shareqq).setOnClickListener(this);
        findViewById(R.id.iv_shareweibo).setOnClickListener(this);
        findViewById(R.id.iv_shareqzone).setOnClickListener(this);
        findViewById(R.id.iv_sharewechatmoments).setOnClickListener(this);
        findViewById(R.id.iv_sharemore).setOnClickListener(this);
        findViewById(R.id.tv_cancel).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_sharewechat:
                if (CommonUtil.isApkExist(context, "com.tencent.mm")) {
                    appLoading.show();
                    ShareUtils.shareToWechat(shareContent, platformActionListener);
//                    fetchAnalysShare(2);
                } else {
                    CutieToast.show(context, R.string.share_nowechatapp);
                }
                this.dismiss();
                break;
            case R.id.iv_shareqq:
                appLoading.show();
                ShareUtils.shareToQQ(shareContent, platformActionListener);
//                fetchAnalysShare(4);
                this.dismiss();
                break;
            case R.id.iv_shareweibo:
                appLoading.show();
//                ShareUtils.startShareToWeibo(context, shareContent, platformActionListener, appLoading);
                ShareUtils.shareToWeiboByOrigin((BaseActivity) context, shareContent, weiboAuthListener);
                this.dismiss();
                break;
            case R.id.iv_shareqzone:
                appLoading.show();
                ShareUtils.shareToQZone(shareContent, platformActionListener);
//                fetchAnalysShare(3);
                this.dismiss();
                break;
            case R.id.iv_sharewechatmoments:
                if (CommonUtil.isApkExist(context, "com.tencent.mm")) {
                    appLoading.show();
                    ShareUtils.shareToWechatMoments(shareContent, platformActionListener);
//                    fetchAnalysShare(1);
                } else {
                    CutieToast.show(context, R.string.share_nowechatapp);
                }
                this.dismiss();
                break;
            case R.id.iv_sharemore:
                appLoading.show();
                ShareUtils.shareToMore(getContext(), BaseConfig.SHARE_URL);
                this.dismiss();
                break;
            case R.id.tv_cancel:
                this.dismiss();
                break;
            default:
                break;
        }
    }


//    /**
//     * 统计分享接口 -- 统计app的分享接口
//     *
//     * @param shareType 朋友圈1，微信好友2，QQ空间3，QQ好友4，新浪微博5
//     */
//    private void fetchAnalysShare(int shareType) {
//        //hasCode==-1 不需要进行回调的处理
//        ZokeParams params = HttpParams.analysShare(-1, 0, shareType);
//        HttpStores.getInstense().post(params);
//    }

    public void dismissLoading() {
        appLoading.dismiss();
    }

    public void setShareContent(ShareContent shareContent) {
        this.shareContent = shareContent;
    }
}

package com.superfish.cutieshop.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.superfish.cutieshop.R;

abstract class BaseBottomDialog extends Dialog {
    protected Context context;
    // 设置位置默认是下
    protected int whichGravity = Gravity.BOTTOM;
    protected int windowAnim;

    public BaseBottomDialog(Context context, int style) {
        super(context, style);
        this.context = context;
        setContentView();
        windowDeploy();
        init();
    }

    public BaseBottomDialog(Context context) {
        super(context, R.style.normarlDialog);
        this.context = context;
        setContentView();
        windowDeploy();
        init();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    protected abstract void setContentView();

    protected abstract void init();

    private void windowDeploy() {
        WindowManager m = ((Activity) context).getWindowManager();
        Display d = m.getDefaultDisplay();
        Window window = getWindow(); //
        window.setWindowAnimations(windowAnim);
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.width = d.getWidth();
        window.setAttributes(wl);
        window.setGravity(whichGravity);
    }

}

package com.superfish.cutieshop.dialog;

import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import com.superfish.cutieshop.R;

/**
 * @author Jack
 */
public class AppLoading extends Dialog {

    private TextView mTips;

    private Context mContext;

    public AppLoading(Context context) {
        super(context, R.style.AppLoadingStyle);
        mContext = context;
        init();
    }


    public AppLoading(Context context, boolean isCancel, int resId) {
        super(context, R.style.AppLoadingStyle);
        mContext = context;
        init();
        setCancelable(isCancel);
        setTips(resId);
    }

    public AppLoading(Context context, int theme) {
        super(context, R.style.AppLoadingStyle);
        mContext = context;
        init();
    }

    //设置提示文字
    public void setTips(int resId) {
        String tip = mContext.getResources().getString(resId);
        mTips.setText(tip);
    }


    /**
     * 初始化
     **/
    protected void init() {
        setContentView(R.layout.dialog_loading);
        mTips = (TextView) findViewById(R.id.loading_tips);
    }
}

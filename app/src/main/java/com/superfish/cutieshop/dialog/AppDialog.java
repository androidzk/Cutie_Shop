package com.superfish.cutieshop.dialog;

import android.content.Context;
import android.widget.TextView;

import com.superfish.cutieshop.R;


/**
 * 普通弹框
 *
 * @author Jack
 */
public class AppDialog extends BaseDialog {

    public AppDialog(Context context) {
        super(context);
    }

    public AppDialog(Context context, boolean cancelable,
                     OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    /**
     * 设置副标题
     **/
    public void setSubTitle(CharSequence text) {
        TextView subTv = new TextView(getContext());
        subTv.setTextSize(16);
        subTv.setText(text);
        subTv.setTextColor(getContext().getResources().getColor(
                R.color.dialog_subtitle));
        setSubContent(subTv);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.dialog_normal;
    }

}

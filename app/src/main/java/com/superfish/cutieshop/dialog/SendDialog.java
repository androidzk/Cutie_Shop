package com.superfish.cutieshop.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superfish.cutieshop.BaseApp;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.activities.LoginActivity;
import com.superfish.cutieshop.adapter.BasePagerAdapter;
import com.superfish.cutieshop.adapter.ChatEmoticonPointIndicatorAdapter;
import com.superfish.cutieshop.adapter.PhaseItemAdapter;
import com.superfish.cutieshop.been.Phase;
import com.superfish.cutieshop.parser.GsonPhase;
import com.superfish.cutieshop.utils.CommonUtil;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.PersistTool;
import com.superfish.cutieshop.utils.TimeUtils;
import com.superfish.cutieshop.view.FixedIndicatorView;
import com.superfish.cutieshop.view.Indicator;
import com.superfish.cutieshop.view.UserDetailRelativeLayout;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 发送窗口
 *
 * @author Jack
 */
public class SendDialog extends BaseBottomDialog implements View.OnClickListener {


    //发送接口
    public interface SendListener {
        void send(String text);

        void dissmiss(String text, boolean isSend);
    }

    private boolean isSend;

    public void setSendListener(SendListener listener) {
        mLisener = listener;
    }

    private SendListener mLisener;

    private Context context;
    private EditText mEdit;
    private TextView mSendTv;

    private UserDetailRelativeLayout mKeyboard;

    private List<Phase> mList;
    private List<GridView> emoticonGridViews;

    private String json;

    public SendDialog(Context context) {
        super(context);
        this.context = context;
    }

    public SendDialog(Context context, SendListener listener) {
        super(context);
        this.context = context;
        mLisener = listener;
    }

    @Override
    public void show() {
        super.show();
        //显示完窗口直接显示键盘
        showKeybroad();
    }

    public void showKeybroad() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        InputMethodManager imm = (InputMethodManager)
                context.getSystemService(context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mEdit, 0); //显示软键盘
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS); //
    }

    public SendDialog(Context context, int style) {
        super(context, style);
        this.context = context;
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.dialog_send);
    }

    @Override
    protected void init() {
        LinearLayout mSendContainer = (LinearLayout) findViewById(R.id.sendContainer);
        mSendContainer.setOnClickListener(this);
        mKeyboard = (UserDetailRelativeLayout) findViewById(R.id.keyboard);
        mEdit = (EditText) findViewById(R.id.edit);
        mSendTv = (TextView) findViewById(R.id.sendBtn);
        mSendTv.setBackgroundResource(TimeUtils.getComments(new Date().getTime()));
    }

    public void setEditContent(String text) {
        mEdit.setText(text);
        mEdit.setSelection(mEdit.getText().length());
    }

    @Override
    public void dismiss() {
        LogTest.wlj("隐藏键盘");
        CommonUtil.hideKeyboard(mEdit);
        //如果有内容需要传递给dialog
        if (mLisener != null)
            mLisener.dissmiss(mEdit.getText().toString(), isSend);
        super.dismiss();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendContainer:
                if (mLisener != null) {
                    String content = mEdit.getText().toString();
                    CommonUtil.hideKeyboard(mEdit);
                    if (TextUtils.isEmpty(content)) {
                        CutieToast.show(context, R.string.global_content_no);
                        return;
                    }
                    if (BaseApp.isGuest()) {
                        LoginActivity.open((Activity) context);
                        return;
                    }
                    if (!CommonUtil.getNetWorkStates(context)) {
                        CutieToast.showNetFailed(context);
                        return;
                    }
                    isSend = true;
                    mLisener.send(mEdit.getText().toString());
                }
                this.dismiss();
                break;
        }
    }
}

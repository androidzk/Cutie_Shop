package com.superfish.cutieshop.dialog;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.superfish.cutieshop.R;
import com.superfish.cutieshop.adapter.RadioAdapter;

import java.util.List;

/**
 * Created by lmh on 15/10/13.
 */
public class RadioDialog extends BaseBottomDialog {
    private ListView lv_radio;
    private TextView tv_cancel;
    private Context mContext;
    private RadioAdapter adapter;
    private List<String> data;

    public void setOnRadioItemClickListener(OnRadioItemClickListener onRadioItemClickListener) {
        this.onRadioItemClickListener = onRadioItemClickListener;
    }

    private OnRadioItemClickListener onRadioItemClickListener;

    public RadioDialog(Context context, List<String> data) {
        super(context);
        mContext = context;
        this.data = data;
        adapter = new RadioAdapter(mContext, data);
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.dialog_radio);
        lv_radio = (ListView) findViewById(R.id.lv_radio);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        lv_radio.setAdapter(adapter);
    }

    @Override
    protected void init() {
        lv_radio.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str = (String) adapter.getItem(position);
                onRadioItemClickListener.onRadioItemClick(str);
            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioDialog.this.dismiss();
            }
        });
    }

    public interface OnRadioItemClickListener {
        public void onRadioItemClick(String str);
    }
}


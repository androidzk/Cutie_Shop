package com.superfish.cutieshop.http;

/**
 * @author JackWu
 */
public interface ZokeCallback {

    /**
     * 请求成功
     **/
    public void onSuccess(ZokeParams out);

    /**
     * 请求失败
     **/
    public void onFails(ZokeParams out);

//	/** 使用缓存 **/
//	public void onCache(ZokeParams out);
}

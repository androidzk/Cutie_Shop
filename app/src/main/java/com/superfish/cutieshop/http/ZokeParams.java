package com.superfish.cutieshop.http;

import com.lidroid.xutils.http.RequestParams;

/**
 * 数据接口参数集
 *
 * @author JackWu
 */
public class ZokeParams extends RequestParams {
    /**
     * 操作类的hashCode
     **/
    private int hashCode;
    /**
     * 操作结果
     **/
    private Object result;
    /**
     * 网络请求Url
     **/
    private String url;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ZokeParams(int hashCode) {
        this.hashCode = hashCode;
    }

    public ZokeParams(int hashCode, String url) {
        this.hashCode = hashCode;
        this.url = url;
    }


    public int getHashCode() {
        return hashCode;
    }

    public void setHashCode(int hashCode) {
        this.hashCode = hashCode;
    }


    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

}

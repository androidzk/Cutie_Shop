package com.superfish.cutieshop.http;

import android.util.SparseArray;

/**
 * 回调缓存集
 *
 * @author JackWu
 */
public class CallBackStores {
    private static CallBackStores mStores = new CallBackStores();
    ;
    private SparseArray<ZokeCallback> mCallbacks = new SparseArray<ZokeCallback>();

    private CallBackStores() {
    }

    public static CallBackStores getCallback() {
        return mStores;
    }

    /**
     * 添加CallBack
     **/
    public synchronized void addCallback(int hashCode, ZokeCallback callback) {
        if (callback != null)
            mCallbacks.put(hashCode, callback);
    }

    /**
     * 移除Callback
     **/
    public synchronized void deleteCallback(int hashCode, ZokeCallback callback) {
        if (callback != null)
            mCallbacks.remove(hashCode);
    }

    /**
     * 清空缓存
     **/
    public synchronized void clearCallbacks() {
        if (null != mCallbacks)
            mCallbacks.clear();
    }

    /**
     * 获取cache大小
     **/
    public synchronized int getCallbackSize() {
        return mCallbacks == null ? 0 : mCallbacks.size();
    }

    /**
     * 更新成功回调方法
     **/
    public synchronized void notifySuccess(ZokeParams out) {
        if (null == out) {
            return;
        }

        if (null == mCallbacks) {
            return;
        }

        // 找到对应的回调 并更新
        int hashCode = out.getHashCode();
        ZokeCallback callback = mCallbacks.get(hashCode);
        if (null == callback) {
            return;
        }
        callback.onSuccess(out);
    }

    /**
     * 更新失败回调方法
     **/
    public synchronized void notifyFaile(ZokeParams out) {
        if (null == out) {
            return;
        }

        if (null == mCallbacks) {
            return;
        }

        // 找到对应的回调 并更新
        int hashCode = out.getHashCode();
        ZokeCallback callback = mCallbacks.get(hashCode);
        if (callback != null)
            callback.onFails(out);
    }


}

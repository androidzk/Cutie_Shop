package com.superfish.cutieshop.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class EmotionViewPager extends ViewPager {

    public EmotionViewPager(Context context) {
        super(context);
    }

    public EmotionViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private float preX;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        try {
            boolean res = super.onInterceptTouchEvent(event);
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                preX = event.getX();
            } else {
                if (Math.abs(event.getX() - preX) > 4) {
                    return true;
                } else {
                    preX = event.getX();
                }
            }
            return res;
        } catch (Exception e) {
            return super.onInterceptTouchEvent(event);
        }

    }

}

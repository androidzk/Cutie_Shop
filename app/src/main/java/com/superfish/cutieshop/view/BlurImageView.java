package com.superfish.cutieshop.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.superfish.cutieshop.utils.Blur;
import com.superfish.cutieshop.utils.BlurListener;


/**
 * 高斯模糊的ImageView -- 使用ImageLoader加载 配合使用
 *
 * @author Jack
 */
public class BlurImageView extends ImageView implements BlurListener {
    private Context mContext;
    private BlurListener mBlurListener;
    private Activity activity;

    public BlurImageView(Context context) {
        super(context);
        this.mContext = context;
        mBlurListener = this;
        activity = (Activity) mContext;
    }

    public BlurImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        mBlurListener = this;
        activity = (Activity) mContext;
    }

    public BlurImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        mBlurListener = this;
        activity = (Activity) mContext;
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        // 异步处理成高斯模糊的 然后掉父类的处理--
        blur(drawable);
    }

    /**
     * 模糊处理
     **/
    private void blur(final Drawable drawable) {
        // 异步任务处理
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final Bitmap bit = Blur.fastblur(mContext,
                            drawableToBitmap(drawable), 5);
                    // final Bitmap bit = BitmapUtils.blur(mContext,
                    // drawableToBitmap(drawable), 5);
                    if (bit == null)
                        return;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mBlurListener.onFinish(bit);
                        }
                    });
                }
            }).start();
        } catch (OutOfMemoryError e) {
            // 不处理任何图片
        }

    }

    /**
     * 处理Drawable转换为Bitmap
     **/
    private Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable == null)
            return null;
        // 取 drawable 的长宽
        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();
        // 取 drawable 的颜色格式
        Bitmap.Config config = drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                : Bitmap.Config.RGB_565;
        // 建立对应 bitmap
        Bitmap bitmap = Bitmap.createBitmap(w, h, config);
        // 建立对应 bitmap 的画布
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, w, h);
        // 把 drawable 内容画到画布中
        drawable.draw(canvas);
        return bitmap;
    }

    @Override
    public void onFinish(Bitmap bit) {
        // 处理结束时回调
        super.setImageDrawable(new BitmapDrawable(bit));
    }

}

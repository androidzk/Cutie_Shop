package com.superfish.cutieshop.view.pulltorefresh.internal;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.superfish.cutieshop.R;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.view.pulltorefresh.PullToRefreshBase.HeaderOrFooter;
import com.superfish.cutieshop.view.pulltorefresh.PullToRefreshBase.Mode;
import com.superfish.cutieshop.view.pulltorefresh.PullToRefreshBase.Orientation;

/**
 * 下拉刷新的动画
 *
 * @author Jack
 */
public class ImageAnimLoadingLayout extends LoadingLayout {

    private AnimationDrawable mAnima;

    public ImageAnimLoadingLayout(Context context, Mode mode,
                                  Orientation scrollDirection, TypedArray attrs,
                                  HeaderOrFooter headerorfooter) {
        super(context, mode, scrollDirection, attrs, headerorfooter);
        // 准备动画
        mAnima = (AnimationDrawable) mHeaderImage.getDrawable();
        mHeaderProgress.setVisibility(View.GONE);// 处理
    }

    @Override
    protected int getDefaultDrawableResId() {
        // 默认的图片
        return R.drawable.pull_loading;
    }

    @Override
    protected void onLoadingDrawableSet(Drawable imageDrawable) {
        // 在Loading加载图片时进行布局的调整 暂时不做处理
    }

    @Override
    protected void onPullImpl(float scaleOfLayout) {
        // 在拉动过程中 返回的拉动值
        LogTest.wlj("scaleOfLayout=" + scaleOfLayout);
        // 开始动画
    }

    @Override
    protected void pullToRefreshImpl() {
        // 准备动画
    }

    @Override
    protected void refreshingImpl() {
        // 正在loading
        mAnima.start();
    }

    @Override
    protected void releaseToRefreshImpl() {
    }

    @Override
    protected void resetImpl() {
        // 重置 恢复初始状态
        mAnima.stop();
        mHeaderImage.clearAnimation();
    }

}

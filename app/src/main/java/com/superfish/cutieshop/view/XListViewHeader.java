package com.superfish.cutieshop.view;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.superfish.cutieshop.R;

public class XListViewHeader extends LinearLayout {
    private LinearLayout mContainer;

    private int mState = STATE_NORMAL;

    private ImageView mLoadingIv;
    private AnimationDrawable mAnima;

    public final static int STATE_NORMAL = 0;
    public final static int STATE_READY = 1;
    public final static int STATE_REFRESHING = 2;

    public XListViewHeader(Context context) {
        super(context);
        initView(context);
    }

    /**
     * @param context
     * @param attrs
     */
    public XListViewHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        // 初始情况，设置下拉刷新view高度为0
        LayoutParams lp = new LayoutParams(
                LayoutParams.MATCH_PARENT, 0);
        mContainer = (LinearLayout) LayoutInflater.from(context).inflate(
                R.layout.xlistview_header, null);
        addView(mContainer, lp);
        setGravity(Gravity.BOTTOM);
        mLoadingIv = (ImageView) findViewById(R.id.loading);
        mAnima = (AnimationDrawable) mLoadingIv.getDrawable();

    }

    public void setState(int state) {
        if (state == mState)
            return;

        if (state == STATE_REFRESHING) { // 显示进度

        } else { // 显示箭头图片

        }

        switch (state) {
            case STATE_NORMAL:
                if (mState == STATE_READY) {
                    mAnima.start();
                }
                if (mState == STATE_REFRESHING) {
                }
                break;
            case STATE_READY:
                if (mState != STATE_READY) {
                }
                break;
            case STATE_REFRESHING:
                break;
            default:
        }

        mState = state;
    }

    public void setVisiableHeight(int height) {
        if (height < 0)
            height = 0;
        LayoutParams lp = (LayoutParams) mContainer
                .getLayoutParams();
        lp.height = height;
        mContainer.setLayoutParams(lp);
    }

    public int getVisiableHeight() {
        return mContainer.getLayoutParams().height;
    }

}

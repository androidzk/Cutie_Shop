package com.superfish.cutieshop.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.superfish.cutieshop.R;

import org.w3c.dom.Text;

/**
 * 弹幕控件基类
 *
 * @author Jack
 */
public abstract class BarrageBase extends RelativeLayout {
    private int position;// 弹幕的位置，在屏幕哪一行

    public ImageView mHead;
    public TextView mContentTv;
    public RelativeLayout mContainer;

    public BarrageBase(Context context) {
        super(context);
        View view = View.inflate(context, R.layout.barrage_item, null);
        mContentTv = (TextView) view.findViewById(R.id.content);
        mHead = (ImageView) view.findViewById(R.id.head);
        mContainer = (RelativeLayout) view.findViewById(R.id.barrageContainer);
        addView(view);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public abstract void send();
}

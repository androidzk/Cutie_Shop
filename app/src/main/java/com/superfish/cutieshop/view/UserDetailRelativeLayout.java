package com.superfish.cutieshop.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * 可感知键盘弹起收起的布局。
 *
 * @author Shangzhi
 */
public class UserDetailRelativeLayout extends RelativeLayout {

    public UserDetailRelativeLayout(Context context) {
        super(context);
    }

    public UserDetailRelativeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UserDetailRelativeLayout(Context context, AttributeSet attrs,
                                    int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (oldw == 0 && oldh == 0) {
            // 系统打开时默认的调用，略过
            super.onSizeChanged(w, h, oldw, oldh);
            return;
        }

        if (h < oldh && h < 0.75 * oldh) {
            // 弹起键盘
            onKeyboardShowEvent(h);
        } else if (oldh < 0.75 * h) {
            onKeyboardHideEvent(h);
        }
        super.onSizeChanged(w, h, oldw, oldh);
    }

    private void onKeyboardHideEvent(int h) {
        // LogTest.sz("我关闭了键盘");
        isKeyboardShowing = false;
        // showBars();
        if (mOnKeyBoardChangeListener != null) {
            mOnKeyBoardChangeListener.onKeyboardHide();
        }
    }

    private void onKeyboardShowEvent(int h) {
        // LogTest.sz("我弹起了键盘");
        isKeyboardShowing = true;
        if (mOnKeyBoardChangeListener != null) {
            mOnKeyBoardChangeListener.onKeyboardShow();
        }
    }

    private boolean isKeyboardShowing = false;

    public boolean isKeyboardShowing() {
        return isKeyboardShowing;
    }

    private OnKeyboardChangeListener mOnKeyBoardChangeListener;

    public void setOnKeyBoardChangeListener(
            OnKeyboardChangeListener onKeyBoardChangeListener) {
        this.mOnKeyBoardChangeListener = onKeyBoardChangeListener;
    }


    /**
     * 键盘事件接口
     */
    public interface OnKeyboardChangeListener {

        void onKeyboardShow();

        void onKeyboardHide();
    }
}

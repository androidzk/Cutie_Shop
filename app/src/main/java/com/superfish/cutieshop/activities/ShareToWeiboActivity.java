package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.been.ShareContent;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.ShareUtils;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;

/**
 * Created by lmh on 15/9/30.
 */
@ContentView(R.layout.activity_sharetoweibo)
public class ShareToWeiboActivity extends BaseActivity {
    private final static String INTENT_SHARECONTENT = "ShareContent";

    @ViewInject(R.id.et_text)
    private TextView et_text;
    @ViewInject(R.id.iv_img)
    private ImageView iv_img;
    @ViewInject(R.id.toolbar)
    private Toolbar mToolbar;

    private ShareContent sc;

    public static void open(Activity act, ShareContent sc) {
        Intent intent = new Intent();
        intent.setClass(act, ShareToWeiboActivity.class);
        intent.putExtra(INTENT_SHARECONTENT, sc);
        open(act, intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolBar();
        sc = (ShareContent) getIntent().getSerializableExtra(INTENT_SHARECONTENT);
        if (sc.getText() != null) {
            et_text.setText(sc.getText());
        }
        if (sc.getImgUrl() != null) {
            ImageLoader.getInstance().displayImage(sc.getImgUrl(), iv_img);
        }

    }

    public void initToolBar() {
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setTitle("");
        setAppTitle(R.string.share_weibo);
        setSupportActionBar(mToolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sharetoweibo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_share) {
            sc.setText("" + et_text.getText());
            ShareUtils.shareToWeibo(sc, new PlatformActionListener() {
                @Override
                public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {

                }

                @Override
                public void onError(Platform platform, int i, Throwable throwable) {

                }

                @Override
                public void onCancel(Platform platform, int i) {

                }
            });
            CutieToast.show(this, R.string.share_finish);
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

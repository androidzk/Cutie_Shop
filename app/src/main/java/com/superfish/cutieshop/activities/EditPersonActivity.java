package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.been.User;
import com.superfish.cutieshop.dialog.AppLoading;
import com.superfish.cutieshop.dialog.GenderDialog;
import com.superfish.cutieshop.http.HttpError;
import com.superfish.cutieshop.http.HttpStores;
import com.superfish.cutieshop.http.ZokeParams;
import com.superfish.cutieshop.parser.GsonLogin;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.HttpParams;
import com.superfish.cutieshop.utils.ImageConfig;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.MessageHandlerList;
import com.superfish.cutieshop.utils.PersistTool;
import com.superfish.cutieshop.utils.TimeUtils;
import com.superfish.cutieshop.utils.UserInfo;

import org.apache.http.protocol.HTTP;
import org.w3c.dom.Text;

/**
 * 修改个人资料
 */
@ContentView(R.layout.activity_edit_person)
public class EditPersonActivity extends BaseActivity {

    @ViewInject(R.id.toolbar)
    private Toolbar mToolbar;
    @ViewInject(R.id.head)
    private ImageView mHead;
    @ViewInject(R.id.tv_nickName)
    private TextView mNickname;
    @ViewInject(R.id.tv_gender)
    private TextView mGender;
    @ViewInject(R.id.container_safe)
    private View mSafeCon;
    @ViewInject(R.id.safe)
    private TextView mSafeTv;

    private User user;
    private AppLoading mLoading;
    private GenderDialog mGenderDialog;

    public static void open(Activity act) {
        Intent i = new Intent(act, EditPersonActivity.class);
        open(act, i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = UserInfo.getInstense(this).getUser();
        mLoading = new AppLoading(this);
        mGenderDialog = new GenderDialog(this);
        mToolbar.setTitle("");
        setAppTitle("编辑个人资料");
        mToolbar.setNavigationIcon(R.drawable.btn_back_bar);
        //动态设置颜色 根据当前星期时间设置
        mToolbar.setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        getStatusBar().setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        setSupportActionBar(mToolbar);
        initUserInfo();

    }

    private void initUserInfo() {
        if (user == null) {
            return;
        }
        ImageLoader.getInstance().displayImage(user.avatar, mHead, ImageConfig.getCirclHeadOptions());
        mNickname.setText(user.nikename);
        int gender = user.gender;
        if (gender == 1) {
            mGender.setText(getResources().getString(R.string.global_gender_male));
        } else if (gender == 2) {
            mGender.setText(getResources().getString(R.string.global_gender_female));
        } else {
            mGender.setText(getResources().getString(R.string.global_gender_unknown));
        }
        mSafeCon.setVisibility(user.isThird == 1 ? View.GONE : View.VISIBLE);
        mSafeTv.setVisibility(user.isThird == 1 ? View.GONE : View.VISIBLE);
    }

    /**
     * 执行修改操作
     **/
    private void fetchEdit(String path) {
        ZokeParams params = HttpParams.getEdit(mHashCode, 0, path, "");
        HttpStores.getInstense().post(params);
    }

    /**
     * 执行修改操作 -- 修改性别
     **/
    private void fetchEdit(int gender) {
        ZokeParams params = HttpParams.getEdit(mHashCode, gender, "", "");
        HttpStores.getInstense().post(params);
    }

    @Override
    public void onSuccess(ZokeParams out) {
        super.onSuccess(out);
        String url = out.getUrl();
        ResponseInfo info = (ResponseInfo) out.getResult();
        String result = (String) info.result;
        if (url.equals(BaseConfig.UrlBank.edit)) {
            LogTest.wlj("result=" + result);
            mLoading.dismiss();
            GsonLogin gl = new GsonLogin().fromJson(result);
            if (gl.info == null || gl.info.user_info == null) {
                //失败了
                initUserInfo();
                CutieToast.show(EditPersonActivity.this, R.string.global_error_accident);
                return;
            }
            user.avatar = gl.info.user_info.avatar;
            user.gender = gl.info.user_info.gender;
            UserInfo.getInstense(EditPersonActivity.this).setUser(user);
            //头像修改成功
            CutieToast.show(EditPersonActivity.this, R.string.edit_ok);
            MessageHandlerList.sendMessage(UserProfileActivity.class, BaseConfig.MessageCode.UPDATE_USER);
            return;
        }
    }

    @Override
    public void onFails(ZokeParams out) {
        super.onFails(out);
        HttpError error = (HttpError) out.getResult();
        LogTest.wlj("error=" + error.s);
        String url = out.getUrl();
        if (url.equals(BaseConfig.UrlBank.edit)) {
            //失败了
            mLoading.dismiss();
            initUserInfo();
            CutieToast.show(EditPersonActivity.this, R.string.global_error_accident);
        }
    }

    @Override
    protected void handleMsg(Message msg) {
        super.handleMsg(msg);
        switch (msg.what) {
            case BaseConfig.MessageCode.CROPER:
                String path = (String) msg.obj;
                ImageLoader.getInstance().displayImage("file:///" + path, mHead, ImageConfig.getCirclHeadOptions());
                //开始弹框 进行头像更改
                mLoading.show();
                fetchEdit(path);
                break;
            case BaseConfig.MessageCode.UPDATE_USER:
                user = UserInfo.getInstense(this).getUser();
                initUserInfo();
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.edit_head)
    public void onClickEditHead(View v) {
        PhotoActivity.open(this);
    }

    @OnClick(R.id.edit_nickname)
    public void onClickEditNickname(View v) {
        EditNameActivity.open(this);
    }

    @OnClick(R.id.container_gender)
    public void onClickEditGender(View v) {
        mGenderDialog.show();
        mGenderDialog.setOnGenderClickListener(new GenderDialog.OnGenderClickListener() {
            @Override
            public void OnGenderClick(String gender) {
                int gen = 0;
                if ("男".equals(gender)) {
                    gen = 1;
                }

                if ("女".equals(gender)) {
                    gen = 2;
                }
                if (gen == 1) {
                    mGender.setText("男");
                } else if (gen == 2) {
                    mGender.setText("女");
                }
                mLoading.show();
                fetchEdit(gen);
            }
        });
    }

    @OnClick(R.id.container_safe)
    public void onClickSafe(View v) {
        EditPasswordActivity.open(this, false);//修改密码
    }

    @OnClick(R.id.exit)
    public void onClickExit(View v) {
        UserInfo.getInstense(EditPersonActivity.this).setUser(null);
        MessageHandlerList.sendMessage(UserProfileActivity.class, BaseConfig.MessageCode.EXIT);
        PersistTool.saveBoolean(BaseConfig.ShareKey.ISLOGIN, false);
        this.finish();
    }


}

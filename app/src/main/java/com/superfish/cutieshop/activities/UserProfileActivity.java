package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseApp;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.adapter.FavoriteAdapter;
import com.superfish.cutieshop.been.Product;
import com.superfish.cutieshop.been.User;
import com.superfish.cutieshop.http.HttpStores;
import com.superfish.cutieshop.http.ZokeParams;
import com.superfish.cutieshop.parser.GsonDetails;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.HttpParams;
import com.superfish.cutieshop.utils.ImageConfig;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.TimeUtils;
import com.superfish.cutieshop.utils.UserInfo;
import com.superfish.cutieshop.view.XListView;

import java.util.ArrayList;
import java.util.List;

@ContentView(R.layout.activity_user_profile)//用户个人信息页面
public class UserProfileActivity extends BaseActivity implements XListView.IXListViewListener, View.OnClickListener {


    private User user;

    /**
     * 开启个人详情页
     **/
    public static void open(Activity act) {
        if (BaseApp.isGuest()) {
            //需要登录
            LoginActivity.open(act);
            return;
        }
        Intent intent = new Intent(act, UserProfileActivity.class);
        open(act, intent);
    }

    @ViewInject(R.id.xListView)
    private XListView mListView;
    private List<Product> mList = new ArrayList<>();
    private FavoriteAdapter mAdapter;


    private View mHeadView;
    private View mSetting;
    private View mBack;
    private View mEditContainer;
    private ImageView mHead;
    private TextView mNameTv;
    private ImageView mGender;

    @ViewInject(R.id.toolbar)
    private Toolbar mToolbar;

    private int mPage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mHeadView = View.inflate(this, R.layout.include_useprofile_header, null);
        super.onCreate(savedInstanceState);
        user = UserInfo.getInstense(this).getUser();
        mSetting = mHeadView.findViewById(R.id.settings);
        mBack = mHeadView.findViewById(R.id.back);
        mEditContainer = mHeadView.findViewById(R.id.edit);
        mToolbar.setTitle("");
        setAppTitle("我的");
        mToolbar.setNavigationIcon(R.drawable.btn_back_bar);
        //动态设置颜色 根据当前星期时间设置
        mToolbar.setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        getStatusBar().setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        setSupportActionBar(mToolbar);
        mHead = (ImageView) mHeadView.findViewById(R.id.head);
        mNameTv = (TextView) mHeadView.findViewById(R.id.name);
        mGender = (ImageView) mHeadView.findViewById(R.id.gender);
        mListView.setPullLoadEnable(true);
        mListView.setPullRefreshEnable(false);
        mListView.addHeaderView(mHeadView);
        mListView.setXListViewListener(UserProfileActivity.this);
        TextView moreTv = mListView.getLoadMoreTextView();
        moreTv.setTextColor(getResources().getColor(R.color.y1_50));
        if (android.os.Build.VERSION.SDK_INT > 8) {// 判断sdk版本
            mListView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        } else {
            mListView.setFadingEdgeLength(0);
        }
        mAdapter = new FavoriteAdapter(this, mList);
        mListView.setAdapter(mAdapter);
        mSetting.setOnClickListener(this);
        mBack.setOnClickListener(this);
        mEditContainer.setOnClickListener(this);
        initHead();
        mPage = 0;
        fetchDetails(mPage);
    }


    /**
     * 获取心愿清单列表
     **/
    private void fetchDetails(int page) {
        ZokeParams params = HttpParams.getDetails(mHashCode, page);
        HttpParams.logParams(params);
        HttpStores.getInstense().get(params);
    }

    @Override
    public void onSuccess(ZokeParams out) {
        super.onSuccess(out);
        String url = out.getUrl();
        ResponseInfo res = (ResponseInfo) out.getResult();
        String result = (String) res.result;
        LogTest.wlj("result=" + result);
        if (url.equals(BaseConfig.UrlBank.details)) {
            GsonDetails gd = new GsonDetails().fromJson(result);
            if (gd.info == null || gd.error_code != 0) {
                CutieToast.show(this, R.string.global_error_accident);
                mListView.stopLoadMore();
                return;
            }

            if (gd.info.list == null ||
                    gd.info.list.length == 0) {
                mListView.stopLoadMore();
                //是否有下一页控制下一页的下拉刷新
                mListView.setPullLoadEnable(gd.info.next_page == 1);
                return;
            }
            //是否有下一页控制下一页的下拉刷新
            mListView.setPullLoadEnable(gd.info.next_page == 1);
            for (Product pd : gd.info.list) {
                mList.add(pd);
            }
            mAdapter.notifyDataSetChanged();
            mListView.stopLoadMore();
            return;
        }
    }

    @Override
    public void onFails(ZokeParams out) {
        super.onFails(out);
        String url = out.getUrl();
        if (url.equals(BaseConfig.UrlBank.details)) {
            CutieToast.showNetFailed(this);
            mListView.stopLoadMore();
            return;
        }

    }

    private void initHead() {
        //初始化User信息
        if (user == null) {
            LogTest.wlj("user==null");
            return;
        }
        ImageLoader.getInstance().displayImage(user.avatar, mHead, ImageConfig.getSqureOptions());
        mNameTv.setText(user.nikename);
        int gen = user.gender;
        mGender.setVisibility(View.VISIBLE);
        if (gen == 1) {
            //男人
            mGender.setImageResource(R.drawable.icon_male);
        } else if (gen == 2) {
            //女人
            mGender.setImageResource(R.drawable.icon_female);
        } else {
            //隐藏 未知
            mGender.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
    }

    @Override
    public void onLoadMore() {
        mPage++;
        fetchDetails(mPage);
    }


    @Override
    protected void handleMsg(Message msg) {
        super.handleMsg(msg);
        switch (msg.what) {
            case BaseConfig.MessageCode.EXIT:
                this.finish();
                break;
            case BaseConfig.MessageCode.UPDATE_USER:
                user = UserInfo.getInstense(UserProfileActivity.this).getUser();
                initHead();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.settings) {
            SettingsActivity.open(this);
            return;
        }

        if (id == R.id.back) {
            this.finish();
            return;
        }

        if (id == R.id.edit) {
            EditPersonActivity.open(this);
            return;
        }
    }
}

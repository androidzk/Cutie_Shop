package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.R;

public class TestLoadingActivity extends BaseActivity {

    public static void open(Activity act) {
        Intent i = new Intent(act, TestLoadingActivity.class);
        open(act, i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_loading);
    }
}

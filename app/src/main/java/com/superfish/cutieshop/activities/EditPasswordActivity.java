package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.dialog.AppLoading;
import com.superfish.cutieshop.http.HttpError;
import com.superfish.cutieshop.http.HttpStores;
import com.superfish.cutieshop.http.ZokeParams;
import com.superfish.cutieshop.parser.GsonLogin;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.HttpParams;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.TimeUtils;
import com.superfish.cutieshop.utils.ValidateUtil;

@ContentView(R.layout.activity_edit_password)
public class EditPasswordActivity extends BaseActivity {

    public static void open(Activity act, boolean isReset) {
        Intent i = new Intent(act, EditPasswordActivity.class);
        i.putExtra("reset", isReset);
        open(act, i);
    }

    /**
     * 默认重置密码
     **/
    public static void open(Activity act) {
        open(act, true);//默认是重置密码
    }

    private boolean isReset;//是否是重置密码
    @ViewInject(R.id.toolbar)
    private Toolbar mToolbar;

    @ViewInject(R.id.edit_password)
    private View mPassword;
    @ViewInject(R.id.password)
    private EditText et_oldpassword;
    @ViewInject(R.id.password1)
    private EditText et_newpassword;
    @ViewInject(R.id.password2)
    private EditText et_confirmpassword;
    private AppLoading appLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appLoading = new AppLoading(this);
        isReset = getIntent().getBooleanExtra("reset", true);
        mToolbar.setTitle("");
        setAppTitle(isReset ? "重置密码" : "修改密码");
        mToolbar.setNavigationIcon(R.drawable.btn_back_bar);
        //动态设置颜色 根据当前星期时间设置
        mToolbar.setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        getStatusBar().setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        setSupportActionBar(mToolbar);
        mPassword.setVisibility(isReset ? View.GONE : View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editpassword, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_share) {
            String oldPassword = et_oldpassword.getText().toString();
            String newPassword = et_newpassword.getText().toString();
            String confirmPassword = et_confirmpassword.getText().toString();
            if (TextUtils.isEmpty(oldPassword)) {
                et_oldpassword.setError(getResources().getString(R.string.password_noempty));
                return true;
            }
            String str_newpassword = ValidateUtil.valPassword(newPassword, this);
            if (!TextUtils.isEmpty(str_newpassword)) {
                et_newpassword.setError(str_newpassword);
                return true;
            }
            if (!newPassword.equals(confirmPassword)) {
                et_confirmpassword.setError(getResources().getString(R.string.validate_passwordnosame));
                return true;
            }
            appLoading.show();
            fetchEditPassword(newPassword, oldPassword);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchEditPassword(String newPassword, String oldPassword) {
        ZokeParams params = HttpParams.editPwd(mHashCode, newPassword, oldPassword);
        HttpStores.getInstense().post(params);
    }

    @Override
    public void onSuccess(ZokeParams out) {
        super.onSuccess(out);
        appLoading.dismiss();
        ResponseInfo result = (ResponseInfo) out.getResult();
        LogTest.lmh("result=" + result.result);
        String url = out.getUrl();
        if (url.equals(BaseConfig.UrlBank.edit_pwd)) {
            String json = (String) result.result;
            GsonLogin gsonLogin = new GsonLogin().fromJson(json);
            if (gsonLogin.error_code == 4005) {
                CutieToast.show(this, getResources().getString(R.string.errorcode_4005));
                return;
            }
            if (gsonLogin.error_code == 4006) {
                CutieToast.show(this, getResources().getString(R.string.errorcode_4006));
                return;
            }
            if (gsonLogin.error_code == 4007) {
                CutieToast.show(this, getResources().getString(R.string.errorcode_4007));
                return;
            }
            if (gsonLogin.info == null || gsonLogin.error_code != 0) {
                //失败
                CutieToast.show(this, getResources().getString(R.string.global_error_accident));
                return;
            }
            CutieToast.show(this, getResources().getString(R.string.editpassword_complete));
            this.finish();
            return;
        }
    }

    @Override
    public void onFails(ZokeParams out) {
        super.onFails(out);
        appLoading.dismiss();
        HttpError error = (HttpError) out.getResult();
        LogTest.lmh("error=" + error.s);
        CutieToast.show(this, getResources().getString(R.string.global_error_internet));
    }
}

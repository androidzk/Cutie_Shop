package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.dialog.AppLoading;
import com.superfish.cutieshop.http.HttpError;
import com.superfish.cutieshop.http.HttpStores;
import com.superfish.cutieshop.http.ZokeParams;
import com.superfish.cutieshop.parser.GsonForgetPwd;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.HttpParams;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.MessageHandlerList;
import com.superfish.cutieshop.utils.TimeUtils;
import com.superfish.cutieshop.utils.ValidateUtil;

/**
 * Created by lmh on 15/10/15.
 */
@ContentView(R.layout.activity_resetpassword2)
public class ResetPassword2Activity extends BaseActivity {
    @ViewInject(R.id.toolbar)
    private Toolbar mToolbar;

    @ViewInject(R.id.et_password)
    private EditText et_password;
    @ViewInject(R.id.et_confirmpassword)
    private EditText et_confirmpassword;
    private String phoneNumber;
    private AppLoading appLoading;

    private static final String INTENT_PHONENUMBER = "intent_phonenumber";

    public static void open(Activity act, String phoneNumber) {
        Intent i = new Intent(act, ResetPassword2Activity.class);
        i.putExtra(INTENT_PHONENUMBER, phoneNumber);
        open(act, i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolBar();
        getExtra();
        appLoading = new AppLoading(this);
    }

    private void getExtra() {
        phoneNumber = getIntent().getStringExtra(INTENT_PHONENUMBER);
    }

    public void initToolBar() {
        mToolbar.setTitle("");
        setAppTitle(R.string.title_resetpassword2);
        mToolbar.setNavigationIcon(R.drawable.btn_back_bar);
        //动态设置颜色 根据当前星期时间设置
        mToolbar.setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        getStatusBar().setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        setSupportActionBar(mToolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_resetpassword2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_share) {
            String password = et_password.getText().toString();
            String confirmPassword = et_confirmpassword.getText().toString();

            String str_password = ValidateUtil.valPassword(password, this);
            if (!TextUtils.isEmpty(str_password)) {
                et_password.setError(str_password);
                return true;
            }

            if (!password.equals(confirmPassword)) {
                et_confirmpassword.setError(getResources().getString(R.string.resetpassword_passwordnosame));
                return true;
            }
            appLoading.show();
            fetchResetPassword(phoneNumber, password);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchResetPassword(String mobile, String new_pwd) {
        ZokeParams params = HttpParams.forgetPwd(mHashCode, mobile, new_pwd);
        HttpStores.getInstense().login(params);
    }

    @Override
    public void onSuccess(ZokeParams out) {
        super.onSuccess(out);
        appLoading.dismiss();
        ResponseInfo result = (ResponseInfo) out.getResult();
        LogTest.lmh("result=" + result.result);
        String url = out.getUrl();
        if (url.equals(BaseConfig.UrlBank.forget_pwd)) {
            String json = (String) result.result;
            GsonForgetPwd gsonForgetPwd = new GsonForgetPwd().fromJson(json);
            if (gsonForgetPwd.info == null || gsonForgetPwd.error_code != 0) {
                //失败
                CutieToast.show(this, getResources().getString(R.string.global_error_accident));
                return;
            }
            if (gsonForgetPwd.info.mobile == null || !gsonForgetPwd.info.mobile.equals(phoneNumber)) {
                CutieToast.show(this, getResources().getString(R.string.global_error_accident));
                return;
            }
            CutieToast.show(this, getResources().getString(R.string.resetpassword_complete));
            MessageHandlerList.sendMessage(ResetPassword1Activity.class, BaseConfig.MessageCode.ON_RESETPASSWORD_COMPLETE);
            this.finish();
            return;
        }
    }

    @Override
    public void onFails(ZokeParams out) {
        super.onFails(out);
        appLoading.dismiss();
        HttpError error = (HttpError) out.getResult();
        LogTest.lmh("error=" + error.s);
        CutieToast.show(this, getResources().getString(R.string.global_error_internet));
    }
}

package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.adapter.GuideAdapter;
import com.superfish.cutieshop.adapter.GuidePointIndicatorAdapter;
import com.superfish.cutieshop.view.FixedIndicatorView;
import com.superfish.cutieshop.view.Indicator;

import java.util.ArrayList;
import java.util.List;

@ContentView(R.layout.activity_guide)
public class GuideActivity extends BaseActivity {

    public static void open(Activity act) {
        Intent i = new Intent(act, GuideActivity.class);
        open(act, i);
    }

    @ViewInject(R.id.vp)
    private ViewPager mViewPager;
    @ViewInject(R.id.indicator)
    private FixedIndicatorView mIndicator;

    private GuidePointIndicatorAdapter mIncatorAdapter;
    private List<Integer> mRedsIds = new ArrayList<>();
    private GuideAdapter mVpAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mRedsIds.add(R.drawable.guide_1);
        mRedsIds.add(R.drawable.guide_2);
        mRedsIds.add(R.drawable.guide_3);
        mRedsIds.add(R.drawable.guide_4);
        mIncatorAdapter = new GuidePointIndicatorAdapter(mRedsIds, this);
        mVpAdapter = new GuideAdapter(this, mRedsIds);
        mViewPager.setAdapter(mVpAdapter);
        mIndicator.setAdapter(mIncatorAdapter);
        mIndicator.setOnItemSelectListener(new Indicator.OnItemSelectedListener() {
            @Override
            public void onItemSelected(View selectItemView, int select, int preSelect) {
                mViewPager.setCurrentItem(select);
            }
        });
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mIndicator.setCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

}

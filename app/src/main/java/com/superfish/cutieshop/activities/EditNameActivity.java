package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.been.User;
import com.superfish.cutieshop.dialog.AppLoading;
import com.superfish.cutieshop.http.HttpStores;
import com.superfish.cutieshop.http.ZokeParams;
import com.superfish.cutieshop.parser.GsonLogin;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.HttpParams;
import com.superfish.cutieshop.utils.MessageHandlerList;
import com.superfish.cutieshop.utils.TimeUtils;
import com.superfish.cutieshop.utils.UserInfo;
import com.superfish.cutieshop.utils.ValidateUtil;

@ContentView(R.layout.activity_edit_name)
public class EditNameActivity extends BaseActivity {

    @ViewInject(R.id.toolbar)
    private Toolbar mToolbar;
    @ViewInject(R.id.tv_nickName)
    private EditText mNameEt;

    private AppLoading mLoading;

    private User user;

    public static void open(Activity act) {
        Intent i = new Intent(act, EditNameActivity.class);
        open(act, i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoading = new AppLoading(this);
        user = UserInfo.getInstense(this).getUser();
        mToolbar.setTitle("");
        setAppTitle("设置昵称");
        mToolbar.setNavigationIcon(R.drawable.btn_back_bar);
        //动态设置颜色 根据当前星期时间设置
        mToolbar.setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        getStatusBar().setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        setSupportActionBar(mToolbar);
        if (user != null)
            mNameEt.setText(user.nikename);
    }

    /**
     * 执行修改操作
     **/
    private void fetchEdit(String name) {
        ZokeParams params = HttpParams.getEdit(mHashCode, 0, "", name);
        HttpStores.getInstense().post(params);
    }

    @Override
    public void onSuccess(ZokeParams out) {
        super.onSuccess(out);
        ResponseInfo info = (ResponseInfo) out.getResult();
        String result = (String) info.result;
        GsonLogin gl = new GsonLogin().fromJson(result);
        if (gl.info == null || gl.info.user_info == null) {
            mLoading.dismiss();
            CutieToast.show(this, R.string.global_error_accident);
            return;
        }
        mLoading.dismiss();
        user.nikename = gl.info.user_info.nikename;
        UserInfo.getInstense(this).setUser(user);
        CutieToast.show(this, R.string.edit_ok);
        MessageHandlerList.sendMessage(EditPersonActivity.class, BaseConfig.MessageCode.UPDATE_USER);
        MessageHandlerList.sendMessage(UserProfileActivity.class, BaseConfig.MessageCode.UPDATE_USER);
        this.finish();

    }

    @Override
    public void onFails(ZokeParams out) {
        super.onFails(out);
        mLoading.dismiss();
        CutieToast.show(this, R.string.global_error_accident);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_register2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_share) {
            //完成修改昵称
            String name = mNameEt.getText().toString();
            String erro = ValidateUtil.valNickname(name, this);
            if (!TextUtils.isEmpty(erro)) {
                mNameEt.setError(erro);
                return true;
            }
            mLoading.show();
            fetchEdit(name);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.superfish.cutieshop.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.adapter.NotifyAdapter;
import com.superfish.cutieshop.been.Notify;
import com.superfish.cutieshop.dialog.AppDialog;
import com.superfish.cutieshop.dialog.BaseDialog;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.SimpleAsync;

import java.util.ArrayList;
import java.util.List;

@ContentView(R.layout.activity_notify)
public class NotifyActivity extends BaseActivity {
    @ViewInject(R.id.lv_notify)
    private ListView lv_notify;
    private NotifyAdapter adapter;
    private List<Notify> datas;
    private DbUtils db;

    public static void open(Activity act) {
        Intent i = new Intent(act, NotifyActivity.class);
        open(act, i);
    }

    @ViewInject(R.id.toolbar)
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = DbUtils.create(NotifyActivity.this);
        initToolBar();
        initListView();
        initDatas();
    }

    private void initDatas() {
        new SimpleAsync(new SimpleAsync.SimpleCallback() {
            @Override
            public void onPrepare() {
                showLoading();
            }

            @Override
            public void onStart() {
                try {
                    datas.clear();
                    List<Notify> tempList = db.findAll(Notify.class);
                    if (tempList != null && tempList.size() != 0) {
                        datas.addAll(tempList);
                    }
                } catch (DbException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFinish() {
                if (datas != null && datas.size() != 0) {
                    showContent();
                } else {
                    showNoContent();
                }
                adapter.notifyDataSetChanged();
            }
        }).execute();

    }

    @Override
    protected void handleMsg(Message msg) {
        super.handleMsg(msg);
        switch (msg.what) {
            case BaseConfig.MessageCode.ON_NEW_UMMESSAGE:
                LogTest.lmh("NotifyActivity-handleMsg-ON_NEW_UMMESSAGE:进入");
                new SimpleAsync(new SimpleAsync.SimpleCallback() {
                    @Override
                    public void onPrepare() {
                    }

                    @Override
                    public void onStart() {
                        try {
                            datas.clear();
                            datas.addAll(db.findAll(Notify.class));
                        } catch (DbException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFinish() {
                        if (datas != null && datas.size() != 0) {
                            showContent();
                        } else {
                            showNoContent();
                        }
                        adapter.notifyDataSetChanged();
                    }
                }).execute();
                break;
            case BaseConfig.MessageCode.ON_READ_UMMESSAGE:
                adapter.notifyDataSetChanged();
                break;
            default:
                break;
        }
    }

    private void initListView() {
        datas = new ArrayList<Notify>();
        adapter = new NotifyAdapter(this, datas);
        lv_notify.setAdapter(adapter);
        lv_notify.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Notify n = (Notify) adapter.getItem(position);
                n.read = BaseConfig.NOTIFICATION_READ;
                try {
                    db.update(n);
                } catch (DbException e) {
                    e.printStackTrace();
                }
                adapter.notifyDataSetChanged();
                WebActivity.open(NotifyActivity.this, n.value, "通知详情", true);

            }
        });
        lv_notify.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final AppDialog appDialog = new AppDialog(NotifyActivity.this);
                appDialog.setDialogTitle("确定要删除吗？");
                appDialog.setLeftText("取消");
                appDialog.setRightText("确定");
                appDialog.setLeftClick(new BaseDialog.LeftClickLinstener() {
                    @Override
                    public void onClickLeft(View view) {
                        appDialog.dismiss();
                    }
                });
                appDialog.setRightClick(new BaseDialog.RightClickLinstener() {
                    @Override
                    public void onClickRight(View view) {
                        try {
                            db.delete(adapter.getItem(position));
                            List<Notify> tempList = db.findAll(Notify.class);
                            if (tempList != null && tempList.size() != 0) {
                                datas.clear();
                                datas.addAll(db.findAll(Notify.class));
                            } else {
                                showNoContent();
                            }
                            adapter.notifyDataSetChanged();
                        } catch (DbException e) {
                            e.printStackTrace();
                        }
                        appDialog.dismiss();
                    }
                });
                appDialog.show();
                return true;
            }
        });
    }

    public void initToolBar() {
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setTitle("");
        setAppTitle(R.string.menu_notify);
        setSupportActionBar(mToolbar);
    }

    public void showLoading() {
        mPageState.showLoading();
        lv_notify.setVisibility(View.GONE);
    }

//    public void showNetFail() {
//        mPageState.showNetFailed();
//        lv_notify.setVisibility(View.GONE);
//        mPageState.setOnReloadListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                mPage = 0;
//                showLoading();
////                fetchMainList(mPage);
//            }
//        });
//    }

    public void showContent() {
        mPageState.hide();
        lv_notify.setVisibility(View.VISIBLE);

    }

    public void showNoContent() {
        mPageState.showNoContent();
        lv_notify.setVisibility(View.GONE);
    }
}
package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.been.User;
import com.superfish.cutieshop.dialog.AppLoading;
import com.superfish.cutieshop.dialog.GenderDialog;
import com.superfish.cutieshop.http.HttpError;
import com.superfish.cutieshop.http.HttpStores;
import com.superfish.cutieshop.http.ZokeParams;
import com.superfish.cutieshop.parser.GsonLogin;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.HttpParams;
import com.superfish.cutieshop.utils.ImageConfig;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.MessageHandlerList;
import com.superfish.cutieshop.utils.PersistTool;
import com.superfish.cutieshop.utils.TimeUtils;
import com.superfish.cutieshop.utils.UID;
import com.superfish.cutieshop.utils.UserInfo;
import com.superfish.cutieshop.utils.ValidateUtil;

/**
 * Created by lmh on 15/10/10.
 */
@ContentView(R.layout.activity_register2)
public class Register2Activity extends BaseActivity {
    @ViewInject(R.id.toolbar)
    private Toolbar mToolbar;
    @ViewInject(R.id.et_nikename)
    private EditText et_nikename;
    @ViewInject(R.id.tv_gender)
    private TextView tv_gender;
    @ViewInject(R.id.iv_avatar)
    private ImageView iv_avatar;

    private String mobile;
    private String password;

    private final static String INTENT_MOBILE = "intent_mobile";
    private final static String INTENT_PASSWORD = "intent_password";

    private GenderDialog genderDialog;
    private String path = "";
    private AppLoading appLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appLoading = new AppLoading(this);
        getExtra();
        initToolBar();
        initGender();
    }

    private void initGender() {
        genderDialog = new GenderDialog(this);
        genderDialog.setOnGenderClickListener(new GenderDialog.OnGenderClickListener() {
            @Override
            public void OnGenderClick(String gender) {
                tv_gender.setText(gender);
            }
        });
    }

    private void getExtra() {
        Intent i = getIntent();
        mobile = i.getStringExtra(INTENT_MOBILE);
        password = i.getStringExtra(INTENT_PASSWORD);
    }

    public void initToolBar() {
        mToolbar.setTitle("");
        setAppTitle(R.string.menu_register1);
        mToolbar.setNavigationIcon(R.drawable.btn_back_bar);
        //动态设置颜色 根据当前星期时间设置
        mToolbar.setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        getStatusBar().setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        setSupportActionBar(mToolbar);
    }

    public static void open(Activity act, String mobile, String password) {
        Intent i = new Intent(act, Register2Activity.class);
        i.putExtra(INTENT_MOBILE, mobile);
        i.putExtra(INTENT_PASSWORD, password);
        open(act, i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_register2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_share) {
            String str_nikename = "";
            str_nikename = ValidateUtil.valNickname(et_nikename.getText().toString(), this);
            if (!TextUtils.isEmpty(str_nikename)) {
                et_nikename.setError(str_nikename);
                return true;
            }
            appLoading.show();
            fetchRegister();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchRegister() {
        ZokeParams params = HttpParams.mobReg(mHashCode, mobile, password, tv_gender.getText().equals(getResources().getString(R.string.register_gender_male)) ? 1 : 2, path, et_nikename.getText().toString());
        HttpStores.getInstense().login(params);
    }

    @Override
    public void onSuccess(ZokeParams out) {
        super.onSuccess(out);
        appLoading.dismiss();
        ResponseInfo result = (ResponseInfo) out.getResult();
        LogTest.lmh("设备登陆返回的结果=" + result.result);
        String url = out.getUrl();
        if (url.equals(BaseConfig.UrlBank.mob_reg)) {
            String json = (String) result.result;
            GsonLogin gsonLogin = new GsonLogin().fromJson(json);
            if (gsonLogin.info == null || gsonLogin.error_code != 0) {
                //失败
                CutieToast.show(this, getResources().getString(R.string.global_error_accident));
                return;
            }
            if (gsonLogin.info.token == null || gsonLogin.info.user_info == null) {
                CutieToast.show(this, getResources().getString(R.string.global_error_accident));
                return;
            }
            User user = gsonLogin.info.user_info;
            user.isThird = 0;
            UserInfo.getInstense(Register2Activity.this).setUser(user);
            UID.updateToken(Register2Activity.this, gsonLogin.info.token);
            PersistTool.saveBoolean(BaseConfig.ShareKey.ISLOGIN, true);
            MessageHandlerList.sendMessage(Register1Activity.class, BaseConfig.MessageCode.ON_REGISTER_COMPLETE);
            MessageHandlerList.sendMessage(LoginActivity.class, BaseConfig.MessageCode.ON_REGISTER_COMPLETE);
            CutieToast.show(this, getResources().getString(R.string.register_complete));
            this.finish();
            return;
        }
    }

    @Override
    public void onFails(ZokeParams out) {
        super.onFails(out);
        HttpError error = (HttpError) out.getResult();
        LogTest.lmh("error=" + error.s);
        CutieToast.show(this, getResources().getString(R.string.global_error_internet));
    }

    @OnClick(R.id.tv_agreement)
    public void onClickAgreement(View view) {
        WebActivity.open(this, BaseConfig.AGREEMENT_URL, "用户协议", true);
    }

    @OnClick(R.id.container_gender)
    public void onClickGender(View view) {
        genderDialog.show();
    }

    @OnClick(R.id.container_avatar)
    public void onClickAvatar(View view) {
        PhotoActivity.open(this);
    }

    @Override
    protected void handleMsg(Message msg) {
        super.handleMsg(msg);
        switch (msg.what) {
            case BaseConfig.MessageCode.CROPER:
                path = (String) msg.obj;
                ImageLoader.getInstance().displayImage("file:///" + path, iv_avatar, ImageConfig.getCirclHeadOptions());
                //开始弹框 进行头像更改
                break;
            default:
                break;
        }
    }
}

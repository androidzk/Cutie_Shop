package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.adapter.PhasePagerAdapter;
import com.superfish.cutieshop.been.PhaseDetail;
import com.superfish.cutieshop.http.HttpError;
import com.superfish.cutieshop.http.HttpStores;
import com.superfish.cutieshop.http.ZokeParams;
import com.superfish.cutieshop.parser.GsonPhaseDetail;
import com.superfish.cutieshop.utils.Blur;
import com.superfish.cutieshop.utils.FontUtils;
import com.superfish.cutieshop.utils.HttpParams;
import com.superfish.cutieshop.utils.LogTest;

import java.util.ArrayList;
import java.util.List;

/**
 * 稿件详情页
 */
@ContentView(R.layout.activity_phase_detail)
public class PhaseDetailActivity extends BaseActivity {
    @ViewInject(R.id.blur_bg)
    private ImageView mBlurBg;
    @ViewInject(R.id.title)
    private TextView mTitle;
    @ViewInject(R.id.subTitle)
    private TextView mSubTitle;
    @ViewInject(R.id.pageNum)
    private TextView mPageNum;
    @ViewInject(R.id.more)
    private TextView mMoreTv;
    @ViewInject(R.id.vp)
    private ViewPager mViewPager;
    private List<PhaseDetail> mList = new ArrayList<>();
    private PhasePagerAdapter mAdapter;
    private String tid;
    private String title;
    private String subTitle;
    private String url;

    private PhaseDetail mDetail;

    public static void open(Activity act, String tid, String title, String subTitle, String url) {
        Intent i = new Intent(act, PhaseDetailActivity.class);
        i.putExtra("tid", tid);
        i.putExtra("title", title);
        i.putExtra("subTitle", subTitle);
        i.putExtra("url", url);
        open(act, i);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tid = getIntent().getStringExtra("tid");
        title = getIntent().getStringExtra("title");
        subTitle = getIntent().getStringExtra("subTitle");
        url = getIntent().getStringExtra("url");
        mTitle.setText(title);
        mSubTitle.setText(subTitle);
        Blur.blurImage(mBlurBg, this, url);
        mPageNum.setTypeface(FontUtils.getTypefaceWithCode(this, 1));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setPageNum(position);
                mDetail = mList.get(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        int pagerWidth = (int) (getResources().getDisplayMetrics().widthPixels * 9.5f / 10.0f);
        mViewPager.measure(0, 0);
        ViewGroup.LayoutParams lp = mViewPager.getLayoutParams();
        if (lp == null) {
            lp = new ViewGroup.LayoutParams(pagerWidth, ViewGroup.LayoutParams.MATCH_PARENT);
        } else {
            lp.width = pagerWidth;
        }
        mViewPager.setLayoutParams(lp);//设置页面宽度为屏幕的3/5
        mViewPager.setOffscreenPageLimit(6);  //设置ViewPager至多缓存4个Pager页面，防止多次加载
//        mViewPager.setPageMargin(50);  //设置Pager之间的间距
        //获取详情
        if (android.os.Build.VERSION.SDK_INT > 8) {// 判断sdk版本
            mViewPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
        } else {
            mViewPager.setFadingEdgeLength(0);
        }
        showLoading();
        fetchPhaseDetail(tid);
    }

    /**
     * 获取详情页
     **/
    private void fetchPhaseDetail(String tid) {
        ZokeParams params = HttpParams.getPhaseDetail(mHashCode, tid);
        HttpStores.getInstense().get(params);
    }


    private void showLoading() {
        mPageState.showLoading();
        mViewPager.setVisibility(View.GONE);
        mPageNum.setVisibility(View.GONE);
        mMoreTv.setVisibility(View.GONE);
    }

    private void showContent() {
        mViewPager.setVisibility(View.VISIBLE);
        mPageState.hide();
        mPageNum.setVisibility(View.VISIBLE);
        mMoreTv.setVisibility(View.VISIBLE);
    }

    private void showNetFail() {
        mPageNum.setVisibility(View.GONE);
        mMoreTv.setVisibility(View.GONE);
        mViewPager.setVisibility(View.GONE);
        mPageState.showNetFailed();
        mPageState.setOnReloadListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading();
                fetchPhaseDetail(tid);
            }
        });
    }


    @Override
    public void onSuccess(ZokeParams out) {
        super.onSuccess(out);
        String url = out.getUrl();
        ResponseInfo responseInfo = (ResponseInfo) out.getResult();
        String result = (String) responseInfo.result;
        LogTest.wlj("result=" + result);
        if (url.equals(BaseConfig.UrlBank.phaseDetail)) {
            GsonPhaseDetail gp = new GsonPhaseDetail().fromJson(result);
            if (gp.info == null || gp.error_code != 0 || gp.info.length == 0) {
                return;
            }
            for (PhaseDetail pd : gp.info) {
                mList.add(pd);
            }
            showContent();
            setPageNum(0);
            mDetail = mList.get(0);
            mAdapter = new PhasePagerAdapter(this, mList, tid);
            mViewPager.setAdapter(mAdapter);
            return;
        }

    }

    @Override
    public void onFails(ZokeParams out) {
        super.onFails(out);
        String url = out.getUrl();
        HttpError error = (HttpError) out.getResult();
        LogTest.wlj("error=" + error.exception.getExceptionCode());
        showNetFail();
    }

    @OnClick(R.id.user)
    public void onClickUser(View v) {
        UserProfileActivity.open(this);
    }

    @OnClick(R.id.back)
    public void onClickBack(View v) {
        this.finish();
    }

    private void setPageNum(int position) {
        mPageNum.setText((position + 1) + "/" + mList.size());
    }

    @OnClick(R.id.more)
    public void onClickMore(View v) {
        //当前是xxx对象
        if (mDetail != null)
            StrategyDetailActivity.open(this, mDetail.title, mDetail.share_url, mDetail.iid, mDetail.detail_counts, mDetail.detail == 1, mDetail.url, !TextUtils.isEmpty(mDetail.open_iid), mDetail.thumb, mDetail.profile);
    }

}

package com.superfish.cutieshop.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseApp;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.http.HttpError;
import com.superfish.cutieshop.http.HttpStores;
import com.superfish.cutieshop.http.ZokeParams;
import com.superfish.cutieshop.parser.GsonLogin;
import com.superfish.cutieshop.utils.HttpParams;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.PersistTool;
import com.superfish.cutieshop.utils.UID;
import com.superfish.cutieshop.utils.UserInfo;

@ContentView(R.layout.activity_splash)
public class SplashActivity extends BaseActivity {
    @ViewInject(R.id.root)
    private View mRoot;
    @ViewInject(R.id.loginStateTv)
    private TextView mLoginStateTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //不支持设置
        mEnableSetStatusBar = false;
        super.onCreate(savedInstanceState);
        mLoginStateTv.setVisibility(View.GONE);
//        initUMengMessage();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        AlphaAnimation anim = new AlphaAnimation(0.4f, 1.0f);
        anim.setDuration(2500);
        anim.setFillAfter(true);
        mRoot.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation animation) {
                if (BaseApp.isLogin()) {
//                    boolean isShowGuide = PersistTool.getBoolean(BaseConfig.ShareKey.SPLASH_KEY, true);
//                    if (isShowGuide) {
//                        //执行showGuide
//                        GuideActivity.open(SplashActivity.this);
//                        PersistTool.saveBoolean(BaseConfig.ShareKey.SPLASH_KEY, false);
//                        return;
//                    }
                    //不处理引导页
                    MainActivity.open(SplashActivity.this);
                    finish();
                    return;
                }
                //未登录 执行登陆操作
                mLoginStateTv.setVisibility(View.VISIBLE);
                mLoginStateTv.setText(getResources().getString(R.string.login_state));
                fetchDeviceLogin();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationStart(Animation animation) {

            }

        });
        //单击此处重新加载
        mRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStateTv.getVisibility() == View.VISIBLE && mLoginStateTv.getText().toString().equals(getResources().getString(R.string.login_failed))) {
                    mLoginStateTv.setVisibility(View.VISIBLE);
                    mLoginStateTv.setText(getResources().getString(R.string.login_state));
                    fetchDeviceLogin();
                }
            }
        });
    }

    /**
     * 执行设备登陆
     **/
    private void fetchDeviceLogin() {
        ZokeParams params = HttpParams.getLoginDevice(mHashCode);
        HttpStores.getInstense().login(params);
    }

    @Override
    public void onSuccess(ZokeParams out) {
        super.onSuccess(out);
        ResponseInfo result = (ResponseInfo) out.getResult();
        LogTest.wlj("设备登陆返回的结果=" + result.result);
        String url = out.getUrl();
        if (url.equals(BaseConfig.UrlBank.login_device)) {
            //设备号登陆
            String json = (String) result.result;
            GsonLogin gsonLogin = new GsonLogin().fromJson(json);
            if (gsonLogin.info == null || gsonLogin.error_code != 0) {
                //失败
                mLoginStateTv.setText(getResources().getString(R.string.login_failed));
                return;
            }
            mLoginStateTv.setVisibility(View.GONE);
            //成功
            String token = gsonLogin.info.token;
            if (!TextUtils.isEmpty(token))
                UID.updateToken(getApplicationContext(), gsonLogin.info.token);
            UserInfo.getInstense(SplashActivity.this).setUser(gsonLogin.info.user_info);
//            boolean isShowGuide = PersistTool.getBoolean(BaseConfig.ShareKey.SPLASH_KEY, true);
//            if (isShowGuide) {
//                //执行showGuide
//                GuideActivity.open(SplashActivity.this);
//                PersistTool.saveBoolean(BaseConfig.ShareKey.SPLASH_KEY, false);
//                return;
//            }
            MainActivity.open(SplashActivity.this);
            finish();
            return;
        }
    }

    @Override
    public void onFails(ZokeParams out) {
        super.onFails(out);
        HttpError error = (HttpError) out.getResult();
        LogTest.wlj("error     =       " + error.exception.getExceptionCode());
        String url = out.getUrl();
        if (url.equals(BaseConfig.UrlBank.login_device)) {
            mLoginStateTv.setText(getResources().getString(R.string.login_failed));
            return;
        }

    }
}

package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.lidroid.xutils.view.annotation.event.OnLongClick;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.utils.CommonUtil;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.TimeUtils;

@ContentView(R.layout.activity_reviews)
public class ReviewsActivity extends BaseActivity {


    public static void open(Activity act) {
        Intent i = new Intent(act, ReviewsActivity.class);
        open(act, i);
    }

    @ViewInject(R.id.toolbar)
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToolbar.setNavigationIcon(R.drawable.btn_back_bar);
        mToolbar.setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        getStatusBar().setBackgroundColor(getResources().getColor(TimeUtils.aTheme()));
        mToolbar.setTitle("");
        setAppTitle(R.string.menu_reviews);
        setSupportActionBar(mToolbar);
    }

    @OnClick(R.id.qqGroup)
    public void onClickQQGroup(View v) {
        //qq一键加群
        Intent intent = new Intent();
        intent.setData(Uri
                .parse("mqqopensdkapi://bizAgent/qm/qr?url=http%3A%2F%2Fqm.qq.com%2Fcgi-bin%2Fqm%2Fqr%3Ffrom%3Dapp%26p%3Dandroid%26k%3D"
                        + "71IyaV94MEt1XvAqS9B1_Xz3HZXcp-V0"));
        // 此Flag可根据具体产品需要自定义，如设置，则在加群界面按返回，返回手Q主界面，不设置，按返回会返回到呼起产品界面
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            startActivity(intent);
        } catch (Exception e) {
            // 未安装手Q或安装的版本不支持
            CutieToast.show(getApplicationContext(), R.string.setttings_qq);
        }
    }

    @OnLongClick(R.id.qqGroup)
    public void onLongClickQQGroup(View v) {
        //长按复制QQ
        CommonUtil.saveTextToClipBoard(ReviewsActivity.this, "332460930");
        CutieToast.show(ReviewsActivity.this, R.string.setttings_copy);
    }

    private String mEmail = "report@ipicopico.com";

    @OnClick(R.id.email)
    public void onClickEmail(View v) {
        try {
            Uri uri = Uri.parse("mailto:" + mEmail);
            String[] email = {mEmail};
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra(Intent.EXTRA_CC, email); // 抄送人
            intent.putExtra(Intent.EXTRA_SUBJECT, ""); // 主题
            intent.putExtra(Intent.EXTRA_TEXT, ""); // 正文
            startActivity(Intent.createChooser(intent, "请选择邮件类应用"));
        } catch (Exception e) {
            //执行多选
            String[] email = {mEmail}; // 需要注意，email必须以数组形式传入
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("message/rfc822"); // 设置邮件格式
            intent.putExtra(Intent.EXTRA_EMAIL, email); // 接收人
            intent.putExtra(Intent.EXTRA_CC, email); // 抄送人
            intent.putExtra(Intent.EXTRA_SUBJECT, ""); // 主题
            intent.putExtra(Intent.EXTRA_TEXT, ""); // 正文
            startActivity(Intent.createChooser(intent, "请选择邮件类应用"));
        }

    }


}

package com.superfish.cutieshop.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.been.ShareContent;
import com.superfish.cutieshop.utils.NotificationUtils;
import com.superfish.cutieshop.utils.ShareUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;

@ContentView(R.layout.test_actvity_lmh)
public class TestLMHActivity extends BaseActivity implements
        OnClickListener, PlatformActionListener {
    @ViewInject(R.id.tv_hint)
    private TextView tv_hint;
    @ViewInject(R.id.toolbar)
    private Toolbar mToolbar;
    private int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setTitle("李鸣鹤测试");
        setSupportActionBar(mToolbar);
        findViewById(R.id.share_wechatmoments).setOnClickListener(this);
        findViewById(R.id.share_wechat).setOnClickListener(this);
        findViewById(R.id.share_qzone).setOnClickListener(this);
        findViewById(R.id.share_weibo).setOnClickListener(this);
        findViewById(R.id.share_qq).setOnClickListener(this);
        findViewById(R.id.btn_crash).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("Test");
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("Test");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.share_wechatmoments:
//                ShareUtils.shareToWechatMoments(ShareContent.getTestShareContent(), this);
//                break;
//            case R.id.share_wechat:
//                ShareUtils.shareToWechat(ShareContent.getTestShareContent(), this);
//                break;
//            case R.id.share_qzone:
//                ShareUtils.shareToQZone(ShareContent.getTestShareContent(), this);
//                break;
//            case R.id.share_weibo:
//                ShareUtils.shareToWeibo(ShareContent.getTestShareContent(), this);
//                MobclickAgent.onEvent(this, BaseConfig.UM.CS_SHARETOWEIBO);
//                break;
//            case R.id.share_qq:
//                ShareUtils.shareToQQ(ShareContent.getTestShareContent(), this);
//                MobclickAgent.onEvent(this, BaseConfig.UM.CS_SHARETOQQ);
//                break;
            case R.id.btn_crash:
//                CrashReport.testJavaCrash();
//                String device_token = UmengRegistrar.getRegistrationId(this);
//                Log.i("lmh", "==========start");
//                Log.i("lmh", device_token);
//                Log.i("lmh", "==========end");
//                Log.i("lmh", getDeviceInfo(this));
//                TaobaoUtils.showTaokeItemDetail(this, TaobaoUtils.TAOBAO);
//                AppDialog appDialog = new AppDialog(this);
//                appDialog.setDialogTitle("标题");
//                appDialog.setContent("内容附近的旅客撒酒疯呆里撒奸分开来的撒酒疯快乐到家萨克拉附近的快乐撒娇罚款了大家快死啦放假快乐的撒娇罚款了电视剧阿卡");
//                appDialog.setLeftText("DISAGREE");
//                appDialog.setRightText("AGREE");
//                appDialog.show();
//                NotificationUtils.notify(this, i++, TestLMHActivity.class);
                break;
            default:
                break;
        }
    }

    public static void openTest(Activity act) {
        Intent intent = new Intent(act, TestLMHActivity.class);
        open(act, intent);
    }

    @Override
    public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
        tv_hint.setText(platform.getName() + "onComplete");
    }

    @Override
    public void onError(Platform platform, int i, Throwable throwable) {
        tv_hint.setText(platform.getName() + "onError");
    }

    @Override
    public void onCancel(Platform platform, int i) {
        tv_hint.setText(platform.getName() + "onCancel");
    }


    public static String getDeviceInfo(Context context) {
        try {
            org.json.JSONObject json = new org.json.JSONObject();
            android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);

            String device_id = tm.getDeviceId();

            android.net.wifi.WifiManager wifi = (android.net.wifi.WifiManager) context.getSystemService(Context.WIFI_SERVICE);

            String mac = wifi.getConnectionInfo().getMacAddress();
            json.put("mac", mac);

            if (TextUtils.isEmpty(device_id)) {
                device_id = mac;
            }

            if (TextUtils.isEmpty(device_id)) {
                device_id = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            }

            json.put("device_id", device_id);

            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;


    }

}



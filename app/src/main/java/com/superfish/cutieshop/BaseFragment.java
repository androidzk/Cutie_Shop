package com.superfish.cutieshop;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superfish.cutieshop.utils.MessageHandlerList;


/**
 * 支持MessageHandlerList的Fragment 统一管理网络请求
 * ps:从Fragment跳转到Activity，将Activity的主题设置为fragment_activity。
 *
 * @author JackWu
 */
public class BaseFragment extends Fragment {
    private Handler mHandler;

    private LayoutInflater inflater;
    private View contentView;
    private ViewGroup container;

    protected int mHashCode;

    protected Intent mIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 仅仅执行一次
        // 设置MessageHandlerList
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                handleMsg(msg);
            }
        };
        String className = this.getClass().getName();
        MessageHandlerList.addHandler(className, mHandler);
        mHashCode = this.hashCode();
        /** 添加回调 **/
//        CallBackStores.getCallback().addCallback(mHashCode, this);
        mIntent = new Intent();
    }

    protected void handleMsg(Message msg) {
        
    }

    /**
     * 获取Handle
     **/
    protected Handler getHandler() {
        return mHandler;
    }

    /**
     * 切换UI 默认动画
     **/
    protected void startUI(Intent intent) {
        startActivity(intent);
    }

    /**
     * 动画方式切换UI
     **/
    protected void startUI(Intent intent, int enterAnim, int exitAnim) {
        getActivity().overridePendingTransition(enterAnim, exitAnim);
        startUI(intent);
    }

    /**
     * 跳转界面ForResult
     **/
    protected void startUIForResult(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    /**
     * title左按钮点击事件
     **/
    public void onTitleClickLeft(View view) {

    }

    /**
     * title标题右按钮点击事件
     **/
    public void onTitleClickRight(View view) {
    }

    /**
     * title标题点击事件
     **/
    public void onTitleClick(View view) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        this.container = container;
        onCreateView(savedInstanceState);
        if (contentView == null)
            return super.onCreateView(inflater, container, savedInstanceState);
        return contentView;
    }

    /**
     * 子类实现该方法 简化fragment添加View的方法 等同于fragment的onCreateView
     **/
    protected void onCreateView(Bundle savedInstanceState) {

    }

    /**
     * 模仿Activity查找控件
     **/
    public View findViewById(int id) {
        return contentView.findViewById(id);
    }


    @Override
    public void onDestroy() {
        // 移除消息池中对应的handler
        String className = this.getClass().getName();
        MessageHandlerList.removeHandler(className);
        /** 移除callBack **/
//        CallBackStores.getCallback().deleteCallback(this.hashCode(), this);
        super.onDestroy();
        contentView = null;
        container = null;
        inflater = null;

    }

//    @Override
//    public void onSuccess(ZokeParams out) {
//
//    }
//
//    @Override
//    public void onFails(ZokeParams out) {
//
//    }

    /**
     * 仿照Acitivity添加布局的方式
     **/
    public void setContentView(int layoutResID) {
        setContentView(inflater.inflate(layoutResID, container, false));
    }

    public void setContentView(View view) {
        contentView = view;
    }

    public View getContentView() {
        return contentView;
    }


}

package com.superfish.cutieshop.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.exception.DbException;
import com.superfish.cutieshop.BaseApp;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.activities.NotifyActivity;
import com.superfish.cutieshop.been.Notify;
import com.superfish.cutieshop.parser.GsonNotify;
import com.superfish.cutieshop.utils.CommonUtil;
import com.superfish.cutieshop.utils.LogTest;
import com.superfish.cutieshop.utils.MessageHandlerList;
import com.superfish.cutieshop.utils.NotificationUtils;
import com.superfish.cutieshop.utils.PersistTool;
import com.umeng.message.UTrack;
import com.umeng.message.UmengBaseIntentService;
import com.umeng.message.entity.UMessage;

import org.android.agoo.client.BaseConstants;
import org.json.JSONObject;

/**
 * Created by lmh on 15/9/18.
 */
public class MyPushIntentService extends UmengBaseIntentService {
    private static final String TAG = "lmh";

// 如果需要打开Activity，请调用Intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)；否则无法打开Activity。

    @Override
    protected void onMessage(Context context, Intent intent) {
        // 需要调用父类的函数，否则无法统计到消息送达
        super.onMessage(context, intent);
        try {
            //可以通过MESSAGE_BODY取得消息体
            String message = intent.getStringExtra(BaseConstants.MESSAGE_BODY);
            UMessage msg = new UMessage(new JSONObject(message));
//            UTrack.getInstance(context).trackMsgClick(msg);
            Log.d(TAG, "message=" + message);    //消息体
            Log.d(TAG, "custom=" + msg.custom);    //自定义消息的内容
            Log.d(TAG, "title=" + msg.title);    //通知标题
            Log.d(TAG, "text=" + msg.text);    //通知内容
            // code  to handle message here
            Toast.makeText(context, msg.custom, Toast.LENGTH_LONG).show();
            LogTest.lmh("Service-onMessage:即将Gson解析");
            GsonNotify gn = new GsonNotify().fromJson(msg.custom);
            LogTest.lmh("Service-onMessage:Gson解析结束");
            Notify n = new Notify();
            n.avatar = gn.send_avatar;
            n.content = gn.send_contents;
            n.read = BaseConfig.NOTIFICATION_UNREAD;
            n.time = gn.send_time;
            n.value = gn.send_value;
            n.type = gn.send_type;
            if (gn.send_type == 1) {
                LogTest.lmh("Service-onMessage:send_type=1");
                NotificationUtils.notifyAndOpenContribution(getApplicationContext(), n);
            } else if (gn.send_type == 2 || gn.send_type == 3) {
                LogTest.lmh("Service-onMessage:send_type=2|3");
                DbUtils db = DbUtils.create(getApplicationContext());
//                Notify n = new Notify();
//                n.avatar = gn.send_avatar;
//                n.content = gn.send_contents;
//                n.read = BaseConfig.NOTIFICATION_UNREAD;
//                n.time = gn.send_time;
//                n.value = gn.send_value;
//                n.type = gn.send_type;
                try {
                    db.save(n);
                } catch (DbException e) {
                    e.printStackTrace();
                }
                NotificationUtils.notifyAndOpenWebPage(context, n);
                LogTest.lmh("SplashActivity-initUMengMessage-dealWithCustomMessage-db.save():成功");
//                MessageHandlerList.sendMessage(NotifyActivity.class, BaseConfig.MessageCode.ON_NEW_UMMESSAGE, n);
                CommonUtil.saveRed(getApplicationContext(), true);
                Intent i = new Intent();
                i.setAction(BaseConfig.BROADCAST_ACTION);
                sendBroadcast(i);
//                PersistTool.saveBoolean(BaseConfig.ShareKey.HAS_NEW_NOTIFY, true);
//                LogTest.lmh("HAS_NEW_NOTIFY置为true:成功");
//                LogTest.lmh("" + PersistTool.getBoolean(BaseConfig.ShareKey.HAS_NEW_NOTIFY, false));
            }
            // 完全自定义消息的处理方式，点击或者忽略
            boolean isClickOrDismissed = true;
            if (isClickOrDismissed) {
                //完全自定义消息的点击统计
                UTrack.getInstance(getApplicationContext()).trackMsgClick(msg);
            } else {
                //完全自定义消息的忽略统计
                UTrack.getInstance(getApplicationContext()).trackMsgDismissed(msg);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }
}
package com.superfish.cutieshop;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.sina.weibo.sdk.api.share.BaseResponse;
import com.sina.weibo.sdk.api.share.IWeiboHandler;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.superfish.cutieshop.http.CallBackStores;
import com.superfish.cutieshop.http.ZokeCallback;
import com.superfish.cutieshop.http.ZokeParams;
import com.superfish.cutieshop.utils.CutieToast;
import com.superfish.cutieshop.utils.MessageHandlerList;
import com.superfish.cutieshop.utils.PageState;
import com.superfish.cutieshop.utils.ScreenManager;
import com.superfish.cutieshop.utils.SystemBarTintManager;
import com.umeng.analytics.MobclickAgent;
import com.umeng.message.PushAgent;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 基类
 *
 * @author 大熊 9月10日
 */
public class BaseActivity extends AppCompatActivity implements ZokeCallback, IWeiboHandler.Response{


    protected static void open(Activity act, Intent intent) {
        act.startActivity(intent);
    }

    protected Handler mHandler;
    /**
     * 请求类对应的hashCode
     **/
    protected int mHashCode;
    /**
     * 是否执行双击返回键退出app 默认不支持
     **/
    protected boolean mEnableDoubleExit = false;
    private boolean mDoubleExit = false;

    protected boolean mEnableSetStatusBar = true;

    private RelativeLayout mStatusBar;

    protected PageState mPageState;


    private TextView mTitle;
    private IWeiboShareAPI mWeiboShareAPI;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PushAgent.getInstance(this).onAppStart();   //友盟消息统计应用启动数据
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                handleMsg(msg);
            }
        };
        initWeibo();
        ViewUtils.inject(this);
        String className = this.getClass().getName();
        MessageHandlerList.addHandler(className, mHandler);
        mHashCode = this.hashCode();
        /** 添加回调 **/
        CallBackStores.getCallback().addCallback(mHashCode, this);
        ScreenManager.getScreenManager().pushActivity(this);
        //支持4.4以上的设置
        RelativeLayout mStatusBar = getStatusBar();
//       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && mEnableSetStatusBar && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            initStatusBar();
            //提供设置占位显示或者隐藏的方法 暴漏给子类进行设置
            if (mStatusBar != null) {
                //只有这个地方需要设置宽高
                mStatusBar.setVisibility(View.VISIBLE);
            }
        } else {
            if (mStatusBar != null) {
                //设置隐藏
                mStatusBar.setVisibility(View.GONE);
            }
        }
        mPageState = new PageState(this);
    }

    private void initWeibo() {
        mWeiboShareAPI = WeiboShareSDK.createWeiboAPI(this, BaseConfig.WEIBO_APPKEY);
        mWeiboShareAPI.registerApp();
    }

    /**
     * 设置标题
     **/
    protected void setAppTitle(String title) {
        mTitle = (TextView) findViewById(R.id.title);
        mTitle.setVisibility(View.VISIBLE);
        mTitle.setText(title);
    }

    /**
     * 设置标题
     **/
    protected void setAppTitle(int resId) {
        String title = getResources().getString(resId);
        setAppTitle(title);
    }


    protected void initStatusBar() {
        //需要获取状态栏的高度
        setTranslucentStatus(true);
        SystemBarTintManager mTintManager = new SystemBarTintManager(this);
        mTintManager.setStatusBarTintEnabled(true);
        mTintManager.setStatusBarAlpha(0.0f);
        mTintManager.setNavigationBarTintEnabled(false);
        //设置底部导航栏的颜色 -- 透明色
        mTintManager.setNavigationBarTintColor(R.color.a1);
        //设置状态栏颜色
        mTintManager.setStatusBarTintColor(R.color.a1);
        mTintManager.setTintColor(R.color.y1);
    }

    //返回当前的StatusBar
    protected RelativeLayout getStatusBar() {
        mStatusBar = (RelativeLayout) findViewById(R.id.status_bar);
        return mStatusBar;
    }


    private void setTranslucentStatus(boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    /**
     * 接收handle的消息
     *
     * @param msg
     */
    protected void handleMsg(Message msg) {
    }

    @Override
    protected void onDestroy() {
        // 移除消息池中对应的handler
        String className = this.getClass().getName();
        MessageHandlerList.removeHandler(className);
        /** 移除callBack **/
        CallBackStores.getCallback().deleteCallback(this.hashCode(), this);
        ScreenManager.getScreenManager().popActivity(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (!mEnableDoubleExit) {
            super.onBackPressed();
            return;
        }
        if (mDoubleExit) {
            super.onBackPressed();
            // 执行到双击退出app
            ((BaseApp) this.getApplicationContext()).exitApp();
            return;
        }
        mDoubleExit = true;
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                mDoubleExit = false;
            }
        }, 2000);
        // 提示的内容让子类去实现
        doubleExitStarted();
    }

    protected void doubleExitStarted() {
        CutieToast.show(BaseActivity.this, R.string.double_exit);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(ZokeParams out) {

    }

    @Override
    public void onFails(ZokeParams out) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);          //统计时长
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mWeiboShareAPI.handleWeiboResponse(intent, this);
    }

    @Override
    public void onResponse(BaseResponse baseResponse) {

    }

    public IWeiboShareAPI getIWeiboShareAPI() {
        return mWeiboShareAPI;
    }
}

package com.superfish.cutieshop.utils;

import java.io.File;

public interface DownloadVersionListener {
	public void onSuccess(File file);
}

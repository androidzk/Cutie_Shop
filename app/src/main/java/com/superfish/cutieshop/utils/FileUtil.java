package com.superfish.cutieshop.utils;

import java.io.File;
import java.text.DecimalFormat;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

/**
 * 
 * @author Jack
 *
 */
public class FileUtil {

	/**
	 * 递归获取某文件夹下的所有文件数
	 * 
	 * @param dirPath
	 * @return
	 */
	public static int getFileCount(String dirPath) {
		int size = 0;
		File dir = new File(dirPath);
		File flist[] = dir.listFiles();
		for (int i = 0; i < flist.length; i++) {
			if (flist[i].isDirectory()) {
				size = size + getFileCount(flist[i].getPath());
			} else {
				size = size + 1;
			}
		}
		return size;
	}

	/**
	 * 递归获取某文件夹下的所有文件大小总和
	 * 
	 * @param dirPath
	 * @return
	 */
	public static long getCacheSize(String dirPath) {
		long size = 0;
		File dir = new File(dirPath);
		File flist[] = dir.listFiles();
		for (int i = 0; i < flist.length; i++) {
			if (flist[i].isDirectory()) {
				size = size + getCacheSize(flist[i].getPath());
			} else {
				size = size + flist[i].length();
			}
		}
		return size;
	}

	/**
	 * 换算文件大小
	 * 
	 * @param size
	 * @return
	 */
	public static String formatFileSize(long size) {
		DecimalFormat df = new DecimalFormat("#.00");
		String fileSizeString = "未知大小";
		if (size < 1024) {
			fileSizeString = df.format((double) size) + "B";
		} else if (size < 1048576) {
			fileSizeString = df.format((double) size / 1024) + "K";
		} else if (size < 1073741824) {
			fileSizeString = df.format((double) size / 1048576) + "M";
		} else {
			fileSizeString = df.format((double) size / 1073741824) + "G";
		}
		return fileSizeString;
	}

	/**
	 * 删除文件夹
	 * 
	 * @param folderPath
	 *            文件夹的路径
	 */
	public static void delFolder(String folderPath) {
		delAllFile(folderPath);
		String filePath = folderPath;
		filePath = filePath.toString();
		File myFilePath = new File(filePath);
		myFilePath.delete();
	}

	/**
	 * 删除文件
	 * 
	 * @param path
	 *            文件的路径
	 */
	public static void delAllFile(String path) {
		File file = new File(path);
		if (!file.exists()) {
			return;
		}
		if (!file.isDirectory()) {
			return;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);
				delFolder(path + "/" + tempList[i]);
			}
		}
	}
	
	/**
	 * 判断应用是否在系统中存在
	 * @param context
	 * @param pageName
	 * @return
	 */
	public static boolean isApkExist(Context context, String pageName) {
		try {
			ApplicationInfo info = context.getPackageManager().getApplicationInfo(
					pageName, PackageManager.GET_UNINSTALLED_PACKAGES);
			return true;
		} catch (NameNotFoundException e) {
			return false;
		} catch (Exception e) {
			return false;
		}

	}

}

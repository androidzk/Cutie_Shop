package com.superfish.cutieshop.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.alibaba.sdk.android.AlibabaSDK;
import com.alibaba.sdk.android.login.LoginService;
import com.alibaba.sdk.android.login.callback.LoginCallback;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.api.share.SendMultiMessageToWeiboRequest;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseApp;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.activities.ShareToWeiboActivity;
import com.superfish.cutieshop.been.ShareContent;
import com.superfish.cutieshop.dialog.AppLoading;
import com.superfish.cutieshop.http.HttpStores;
import com.superfish.cutieshop.http.ZokeParams;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.Platform.ShareParams;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

public class ShareUtils {

    public static void initShareSDK(Context context) {
        ShareSDK.initSDK(context.getApplicationContext());
    }

    public static void shareToQQ(ShareContent sc,
                                 PlatformActionListener platformActionListener) {
        Platform platfrom = ShareSDK.getPlatform(QQ.NAME);
        platfrom.setPlatformActionListener(platformActionListener);
        ShareParams shareParams = new ShareParams();
        shareParams.setTitle(sc.getTitle());
        shareParams.setTitleUrl(sc.getUrl());
        shareParams.setText(sc.getText());
        shareParams.setImageUrl(sc.getImgUrl());
        platfrom.share(shareParams);
        fetchAnalysShare(4, sc.getIid());
    }

    public static void shareToQZone(ShareContent sc,
                                    PlatformActionListener platformActionListener) {
        Platform platfrom = ShareSDK.getPlatform(QZone.NAME);
        ShareParams shareParams = new ShareParams();
        shareParams.setTitle(sc.getTitle());
        shareParams.setTitleUrl(sc.getUrl());
        shareParams.setText(sc.getText());
        shareParams.setSite(sc.getSite());
        shareParams.setSiteUrl(sc.getUrl());
        shareParams.setImageUrl(sc.getImgUrl());
        platfrom.share(shareParams);
        platfrom.setPlatformActionListener(platformActionListener);
        fetchAnalysShare(3, sc.getIid());
    }

    public static void shareToWechat(ShareContent sc,
                                     PlatformActionListener platformActionListener) {
        Platform platfrom = ShareSDK.getPlatform(Wechat.NAME);
        ShareParams shareParams = new ShareParams();
        shareParams.setShareType(Platform.SHARE_WEBPAGE);
        shareParams.setTitle(sc.getTitle());
        shareParams.setText(sc.getText());
        shareParams.setImageUrl(sc.getImgUrl());
        shareParams.setUrl(sc.getUrl());
        platfrom.share(shareParams);
        platfrom.setPlatformActionListener(platformActionListener);
        fetchAnalysShare(2, sc.getIid());
    }

    public static void shareToWechatMoments(ShareContent sc,
                                            PlatformActionListener platformActionListener) {
        Platform platfrom = ShareSDK.getPlatform(WechatMoments.NAME);
        ShareParams shareParams = new ShareParams();
        shareParams.setShareType(Platform.SHARE_WEBPAGE);
        shareParams.setTitle(sc.getTitle());
        shareParams.setText(sc.getText());
        shareParams.setImageUrl(sc.getImgUrl());
        shareParams.setUrl(sc.getUrl());
        platfrom.share(shareParams);
        platfrom.setPlatformActionListener(platformActionListener);
        fetchAnalysShare(1, sc.getIid());
    }

    public static void shareToWeibo(ShareContent sc,
                                    PlatformActionListener platformActionListener) {
//        Platform platfrom = ShareSDK.getPlatform(SinaWeibo.NAME);
//        ShareParams shareParams = new ShareParams();
//        String url = sc.getUrl();
//        if (url != null) {
//            shareParams.setText(sc.getText() + "\n" + sc.getUrl());
//        } else {
//            shareParams.setText(sc.getText());
//        }
//        shareParams.setImageUrl(sc.getImgUrl());
//        platfrom.share(shareParams);
//        platfrom.setPlatformActionListener(platformActionListener);
//        fetchAnalysShare(5, sc.getIid());
    }

    public static void shareToMore(Context context, String text) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain"); // 纯文本
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(Intent.createChooser(intent, null));
    }

    public static void startShareToWeibo(Context context, ShareContent sc, PlatformActionListener platformActionListener, AppLoading appLoading) {
        if (CommonUtil.isApkExist(context, "com.sina.weibo")) {
            ShareUtils.shareToWeibo(sc, platformActionListener);
        } else {
            ShareToWeiboActivity.open((Activity) context, sc);
        }
        appLoading.dismiss();
    }

    public static void thirdPartyLogin(String platfromName, PlatformActionListener platformActionListener) {
        Platform platform = ShareSDK.getPlatform(platfromName);
        platform.setPlatformActionListener(platformActionListener);
        platform.SSOSetting(false);
        if (platform.isValid()) {
            platform.removeAccount();
        }
        platform.showUser(null);
    }

    public static void taobaoLogin(Activity activity, LoginCallback loginCallback) {
        //调用getService方法来获取服务
        LoginService loginService = AlibabaSDK.getService(LoginService.class);
        loginService.showLogin(activity, loginCallback);
    }

    /**
     * 统计商品的对外分享
     *
     * @param share 分享渠道 ， 朋友圈1，微信好友2，QQ空间3，QQ好友4，新浪微博5
     * @param iid   稿件的对应 tid，兼容分享 app ,app 时传递的值为0
     */
    private static void fetchAnalysShare(int share, int iid) {
        //hasCode==-1 不需要进行回调的处理
        ZokeParams params = HttpParams.itemShare(-1, share, iid);
        HttpUtils mHttp = HttpStores.getInstense().getHttp();
        params.setHeader("token", BaseApp.getNextUidToken());
        mHttp.send(HttpRequest.HttpMethod.POST, params.getUrl(), params, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                LogTest.lmh("统计分享成功" + responseInfo.result);
            }

            @Override
            public void onFailure(HttpException e, String s) {
                LogTest.lmh("统计分享失败");
            }
        });
    }

    /**
     * 微博授权
     */
    public static void weiboLogin(SsoHandler mSsoHandler, WeiboAuthListener listener) {
        mSsoHandler.authorize(listener);
    }

    public static void shareToWeiboByOrigin(final BaseActivity activity, ShareContent sc, WeiboAuthListener weiboAuthListener) {
        WeiboMultiMessage weiboMessage = new WeiboMultiMessage();//初始化微博的分享消息
        TextObject textObject = new TextObject();
        textObject.text = sc.getText();
        weiboMessage.textObject = textObject;
        ImageObject imageObject = new ImageObject();
        imageObject.imagePath = sc.getImgUrl();
        weiboMessage.imageObject = imageObject;
        SendMultiMessageToWeiboRequest request = new SendMultiMessageToWeiboRequest();
        request.transaction = String.valueOf(System.currentTimeMillis());
        request.multiMessage = weiboMessage;

        AuthInfo authInfo = new AuthInfo(activity, BaseConfig.WEIBO_APPKEY, BaseConfig.WEIBO_REDIRECTURL, BaseConfig.WEIBO_SCOPE);
        Oauth2AccessToken accessToken = AccessTokenKeeper.readAccessToken(activity.getApplicationContext());
        String token = "";
        if (accessToken != null) {
            token = accessToken.getToken();
        }
//        activity.getIWeiboShareAPI().sendRequest(activity, request, authInfo, token, new WeiboAuthListener() {
//
//            @Override
//            public void onWeiboException(WeiboException arg0) {
//
//            }
//
//            @Override
//            public void onComplete(Bundle bundle) {
//                Oauth2AccessToken newToken = Oauth2AccessToken.parseAccessToken(bundle);
//                AccessTokenKeeper.writeAccessToken(activity.getApplicationContext(), newToken);
//                CutieToast.show(activity, "微博分享成功");
//            }
//
//            @Override
//            public void onCancel() {
//                CutieToast.show(activity, "微博分享取消");
//            }
//        });
        activity.getIWeiboShareAPI().sendRequest(activity, request, authInfo, token, weiboAuthListener);
        fetchAnalysShare(5, sc.getIid());
    }
}

package com.superfish.cutieshop.utils;

import android.util.Log;

import com.lidroid.xutils.http.ResponseInfo;

/**
 * Log
 */
public class LogTest {

    private static final boolean isDebug = true;

    public static final String TAG_WLJ = "wlj";
    public static final String TAG_LMH = "lmh";


    public static void lmh(String msg) {
        i(TAG_LMH, msg);
    }

    public static ResponseInfo wlj(String msg) {
        i(TAG_WLJ, msg);
        return null;
    }

    public static void i(String tag, String msg) {
        if (isDebug) {
            Log.i(tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (isDebug) {
            Log.e(tag, msg);
        }
    }

}

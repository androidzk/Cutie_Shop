package com.superfish.cutieshop.utils;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.widget.ImageView;

import com.superfish.cutieshop.BaseActivity;
import com.superfish.cutieshop.BaseFragment;
import com.superfish.cutieshop.R;

/**
 * 页面状态处理工具
 *
 * @author Jack
 */
public class PageState {
    protected BaseActivity mBaseAct;
    protected BaseFragment mBaseFragment;
    protected Context mContext;
    private AnimationDrawable mAnima;

    public PageState(BaseActivity baseAct) {
        this.mBaseAct = baseAct;
        mContext = baseAct;
    }

    public PageState(BaseFragment baseAct) {
        this.mBaseFragment = baseAct;
        mContext = baseAct.getActivity();
    }


    /**
     * 获取PageState的容器
     **/
    public View getContainer() {
        return mBaseFragment == null ? mBaseAct
                .findViewById(R.id.pageState) : mBaseFragment
                .findViewById(R.id.pageState);
    }

    /**
     * 隐藏页面状态处理
     **/
    public void hide() {
        View v = getContainer();
        if (v != null)
            v.setVisibility(View.GONE);
    }

    public void show() {
        View v = getContainer();
        if (v != null)
            v.setVisibility(View.VISIBLE);
    }

    public View getWithoutData() {
        return mBaseFragment == null ? mBaseAct
                .findViewById(R.id.noContent) : mBaseFragment
                .findViewById(R.id.noContent);
    }

    public View getNetFailed() {
        return mBaseFragment == null ? mBaseAct
                .findViewById(R.id.netFailed) : mBaseFragment
                .findViewById(R.id.netFailed);
    }

    public View getLoadingContainer() {
        return mBaseFragment == null ? mBaseAct
                .findViewById(R.id.loadingContainer) : mBaseFragment
                .findViewById(R.id.loadingContainer);
    }

    public ImageView getLoading() {
        return mBaseFragment == null ? (ImageView) mBaseAct
                .findViewById(R.id.loading) : (ImageView) mBaseFragment
                .findViewById(R.id.loading);
    }

    public View getReload() {
        return mBaseFragment == null ? mBaseAct
                .findViewById(R.id.reLoad) : mBaseFragment
                .findViewById(R.id.reLoad);
    }

    /**
     * 设置重新加载的点击事件
     *
     * @param listener
     */
    public void setOnReloadListener(View.OnClickListener listener) {
        View reload = getReload();
        if (reload != null)
            reload.setOnClickListener(listener);
    }

    /**
     * 显示暂无数据
     **/
    public void showNoContent() {
        show();
        View noData = getWithoutData();
        View netFail = getNetFailed();
        View loading = getLoadingContainer();
        if (noData != null)
            noData.setVisibility(View.VISIBLE);
        if (netFail != null)
            netFail.setVisibility(View.GONE);
        if (loading != null)
            loading.setVisibility(View.GONE);

    }

    /**
     * 显示网络加载失败
     **/
    public void showNetFailed() {
        show();
        View noData = getWithoutData();
        View netFail = getNetFailed();
        View loading = getLoadingContainer();
        if (noData != null)
            noData.setVisibility(View.GONE);
        if (netFail != null)
            netFail.setVisibility(View.VISIBLE);
        if (loading != null)
            loading.setVisibility(View.GONE);
    }

    /**
     * 显示正在加载
     **/
    public void showLoading() {
        show();
        View noData = getWithoutData();
        View netFail = getNetFailed();
        View loadingC = getLoadingContainer();
        ImageView loading = getLoading();
        if (noData != null)
            noData.setVisibility(View.GONE);
        if (netFail != null)
            netFail.setVisibility(View.GONE);
        if (loading != null) {
            loadingC.setVisibility(View.VISIBLE);
            loading.setVisibility(View.VISIBLE);
            mAnima = (AnimationDrawable) loading.getDrawable();
            if (mAnima != null) {
                mAnima.stop();
                loading.clearAnimation();
                mAnima.start();
            }
        }

    }


}

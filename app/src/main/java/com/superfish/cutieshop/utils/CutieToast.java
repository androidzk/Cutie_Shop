package com.superfish.cutieshop.utils;

import android.content.Context;
import android.widget.Toast;

import com.superfish.cutieshop.R;

/**
 * 封装toast 便于后续变更toast效果
 */
public class CutieToast {

    public static void show(Context context, CharSequence s) {
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
    }

    public static void show(Context context, int strId) {
        show(context, context.getResources().getString(strId));
    }


    /**
     * 没有网络的提醒
     *
     * @param context
     */
    public static void showNetFailed(Context context) {
        show(context, R.string.toast_show_netfail);
    }

    /**
     * 没有内容的提醒
     *
     * @param context
     */
    public static void showNoMore(Context context) {
        show(context, R.string.toast_show_nocontent);
    }

}

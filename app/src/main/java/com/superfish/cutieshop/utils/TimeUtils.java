package com.superfish.cutieshop.utils;

import com.superfish.cutieshop.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 *
 * @author Jack
 */
public class TimeUtils {
    public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat dayFormat = new SimpleDateFormat("MM月dd日");
    public static String[] weeks = {"", "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六",};
    public static String[] jpweeks = {"", "日曜日", "月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日",};
    public static int[] acolors = {0, R.color.a7, R.color.a1, R.color.a2, R.color.a3, R.color.a4, R.color.a5, R.color.a6,};
    public static int[] bcolors = {0, R.color.b7, R.color.b1, R.color.b2, R.color.b3, R.color.b4, R.color.b5, R.color.b6,};
    public static int[] ccolors = {0, R.color.c7, R.color.c1, R.color.c2, R.color.c3, R.color.c4, R.color.c5, R.color.c6,};
    public static int[] icons = {0, R.drawable.strip_home_07, R.drawable.strip_home_01, R.drawable.strip_home_02, R.drawable.strip_home_03, R.drawable.strip_home_04, R.drawable.strip_home_05, R.drawable.strip_home_06,};
    public static int[] counts = {0, R.drawable.shape_a7, R.drawable.shape_a1, R.drawable.shape_a2, R.drawable.shape_a3, R.drawable.shape_a4, R.drawable.shape_a5, R.drawable.shape_a6};
    public static int[] comments = {0, R.drawable.shape_comment7, R.drawable.shape_comment1, R.drawable.shape_comment2, R.drawable.shape_comment3, R.drawable.shape_comment4, R.drawable.shape_comment5, R.drawable.shape_comment6};
    public static int[] heads = {0, R.drawable.shape_head7, R.drawable.shape_head1, R.drawable.shape_head2, R.drawable.shape_head3, R.drawable.shape_head4, R.drawable.shape_head5, R.drawable.shape_head6};
    public static int[] mbs = {0, R.drawable.mb7, R.drawable.mb1, R.drawable.mb2, R.drawable.mb3, R.drawable.mb4, R.drawable.mb5, R.drawable.mb6};

    /**
     * 获取一个时间是星期几 -- 时间单位是毫秒
     **/
    public static String getWeekDay(long time) {
        return weeks[getWeekPosition(time)];
    }

    /**
     * 获取日本星期
     *
     * @param time
     * @return
     */
    public static String getJpWeekDay(long time) {
        return jpweeks[getWeekPosition(time)];
    }


    /** **/
    private static int getWeekPosition(long time) {
        String day = formatter.format(time);
        String[] days = day.split("-");
        Calendar calendar = Calendar.getInstance();//获得一个日历
        calendar.set(Integer.parseInt(days[0]), Integer.parseInt(days[1]) - 1, Integer.parseInt(days[2]));//设置当前时间,月份是从0月开始计算
        int number = calendar.get(Calendar.DAY_OF_WEEK);//星期表示1-7，是从星期日开始
        return number;
    }


    /**
     * 获取蒙版
     **/
    public static int getMbRes(long time) {
        return mbs[getWeekPosition(time)];
    }

    /**
     * 获取蒙版
     **/
    public static int getMbRes() {
        return getMbRes(new Date().getTime());
    }

    /**
     * 根据当前日期获取该星期应该使用的颜色
     **/
    public static int aTheme() {
        return aTheme(new Date().getTime());
    }

    /**
     * 根据当前日期获取该星期应该使用的颜色
     **/
    public static int aTheme(long time) {
        return acolors[getWeekPosition(time)];
    }


    /**  **/
    public static int cTheme(long time) {
        return ccolors[getWeekPosition(time)];
    }

    /**
     * 根据日期获取图标
     **/
    public static int getIcons(long time) {
        return icons[getWeekPosition(time)];
    }


    /**
     * 获取图标
     **/
    public static int getCounts(long time) {
        return counts[getWeekPosition(time)];
    }

    /**
     * 获取图标
     **/
    public static int getComments(long time) {
        return comments[getWeekPosition(time)];
    }

    public static int getHeads(long time) {
        return heads[getWeekPosition(time)];
    }


    /**
     * 根据当前日期获取该星期应该使用的颜色
     **/
    public static int bTheme() {
        return bTheme(new Date().getTime());
    }

    /**
     * 根据当前日期获取该星期应该使用的颜色 单位毫秒
     **/
    public static int bTheme(long time) {
        return bcolors[getWeekPosition(time)];
    }

    /**
     * 是否相同 单位秒
     **/
    public static boolean isSame(long preTime, long nextTime) {
        String preDay = formatter.format(preTime * 1000);
        String nextDay = formatter.format(nextTime * 1000);
        return preDay.equals(nextDay);
    }

    /**
     * 格式化时间 按照9月15日的格式来 单位秒
     **/
    public static String formatTime(long time) {
        return dayFormat.format(time * 1000);
    }


    /**
     * @return 返回时间字符串，如"刚刚"，"1分钟前" 单位秒
     */
    public static String getTimeStr(long update_time) {
        long nowTime = new Date().getTime() / 1000;
        long timeDiff = nowTime - update_time;
        if (timeDiff < 0) {
            // 发布时间比现在还晚，时间有问题
            return "  刚刚  ";
        } else {
            if (timeDiff < 600) {
                return "  刚刚  ";
            } else if (timeDiff < 3600) {
                return timeDiff / 60 + "分钟前";
            } else if (timeDiff < 3600 * 24) {
                return timeDiff / 3600 + "小时前";
            } else {
                // 比24小时还大
                Date timeDate = new Date(update_time * 1000);
                // 判断此日期是不是今天 如果是今天 只显示日期 如果不是今天 则显示日子
                SimpleDateFormat formatter = new SimpleDateFormat("MM月dd日");
                return formatter.format(timeDate);
            }
        }
    }


    /**
     * 单位毫秒秒 分割年月日
     **/
    public static String[] splitTime(long time) {
        String fTime = formatter.format(time);
        String[] times = fTime.split("-");
        return times;
    }

    public static String getMonth(long time) {
        String[] times = splitTime(time);
        String month = times[1];
        int num = Integer.parseInt(month);
        return months[num - 1];
    }

    private static String[] months = {"一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"};


}

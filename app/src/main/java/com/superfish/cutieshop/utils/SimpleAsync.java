package com.superfish.cutieshop.utils;

import android.os.AsyncTask;

/**
 * 
 * 简单异步
 * 
 * @author JackWu
 * 
 * 
 */
public class SimpleAsync extends AsyncTask<Void, Long, Object> {
	private SimpleCallback mCallback;

	public SimpleAsync(SimpleCallback callback) {
		setCallback(callback);
	}

	@Override
	protected Object doInBackground(Void... voids) {
		if (mCallback != null) {
			mCallback.onStart();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
		if (mCallback != null) {
			mCallback.onFinish();
		}
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (mCallback != null) {
			mCallback.onPrepare();
		}
	}

	public void setCallback(SimpleCallback callback) {
		this.mCallback = callback;
	}

	public interface SimpleCallback {
		public void onPrepare();

		public void onStart();

		public void onFinish();
	}
}

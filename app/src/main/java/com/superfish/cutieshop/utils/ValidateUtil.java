package com.superfish.cutieshop.utils;

import android.content.Context;
import android.text.TextUtils;

import com.superfish.cutieshop.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 校验
 * 
 * @author Jack
 *
 */
public class ValidateUtil {

	/** 密码的校验规则 英文+数字 不支持空格和符号 **/
	public static final String passRelex = "^[A-Za-z0-9\\-]+$";
	/** 昵称校验 包含中文 **/
	public static final String nickNameRelex = "^[\u4e00-\u9fa5]+$";

	/** 正则校验 **/
	public static boolean validate(String context, String passRelex) {
		Pattern pat = Pattern.compile(passRelex);
		Matcher mat = pat.matcher(context);
		return mat.find();
	}

	/**
	 * 校验密码
	 * 
	 * @param password
	 * @param context
	 * @return 如果返回空字符串 则校验通过
	 */
	public static String valPassword(String password, Context context) {
		String val = "";
		// 验证密码不能为空
		if (TextUtils.isEmpty(password))
			return getStr(R.string.password_noempty, context);
		// 验证为6-14位 长度
		int lenth = password.length();
		if (lenth <= 5 || lenth >= 19)
			return getStr(R.string.password_limitlength, context);
		// 验证是否只是英文（不区分大小写）和数字
		if (!validate(password, passRelex))
			return getStr(R.string.password_gs, context);
		return val;
	}

	public static String getStr(int resId, Context context) {
		return context.getResources().getString(resId);
	}

	/** 校验手机号 **/
	public static String valPhone(String phone, Context context) {
		String val = "";
		// 手机号码不能为空
		if (TextUtils.isEmpty(phone))
			return getStr(R.string.sms_forget_title_one, context);
		// 考虑到不同国家手机号码不同这里只做简单的位数检测
		int lenth = phone.length();
		if (lenth > 0 && lenth < 11) {
			return getStr(R.string.smssdk_write_right_mobile_phone, context);
		}
		return val;
	}

	/** 校验该文字中有多少个中文 **/
	@SuppressWarnings("static-access")
	public static int pattenChinse(String nickName) {
		nickName = new String(nickName.getBytes());// GBK编码
		Pattern p = Pattern.compile(nickNameRelex);
		int x = 0;
		for (int i = 0; i < nickName.length(); i++) {
			if (p.matches(nickNameRelex, nickName.substring(i, i + 1))) {
				x++;
			}
		}
		return x;
	}

	/**
	 * 校验用户昵称 （7个中文 14个英文 最多状态 不可空格 可符号)
	 * 
	 * @param nickName
	 * @param context
	 * @return
	 */
	public static String valNickname(String nickName, Context context) {
		String val = "";
		if (TextUtils.isEmpty(nickName))
			return getStr(R.string.nickname_noempty, context);
		if (nickName.contains(" "))
			return getStr(R.string.nickname_nokg, context);
		if (nickName.length() > 14)
			return getStr(R.string.nickname_limitlenth, context);// 不做任何匹配都超过14个字符了
																	// 直接提示
		int cCount = pattenChinse(nickName);// 该中文中包含的中文的个数
		// 部分中文 部分英文
		int enCount = nickName.length() - cCount;
		if ((enCount + cCount * 2) > 14)
			return getStr(R.string.nickname_limitlenth, context);
		return val;
	}

	/**
	 * 校验用户昵称 （7个中文 14个英文 最多状态 不可空格 可符号)
	 * 
	 * @param nickName
	 * @param context
	 * @return
	 */
	public static String valNickname2(String nickName, Context context) {
		String val = "";
		if (TextUtils.isEmpty(nickName))
			return "empty";
		if (nickName.contains(" "))
			return getStr(R.string.nickname_nokg, context);
		if (nickName.length() > 14)
			return getStr(R.string.nickname_limitlenth, context);// 不做任何匹配都超过14个字符了
																	// 直接提示
		int cCount = pattenChinse(nickName);// 该中文中包含的中文的个数
		// 部分中文 部分英文
		int enCount = nickName.length() - cCount;
		if ((enCount + cCount * 2) > 14)
			return getStr(R.string.nickname_limitlenth, context);
		return val;
	}

//	/** 校验token是否失效 **/
//	public static boolean checkToken(int erroCode, Context context) {
//		if (erroCode == 90004) {
//			// 执行xxx校验失败的结果
//			LogTest.e("token校验失败了");
//			MessageHandlerList.sendMessage(MainActivity.class,
//					MessageCode.TOKEN_FAILED);
//			return true;
//		}
//		return false;
//	}

}

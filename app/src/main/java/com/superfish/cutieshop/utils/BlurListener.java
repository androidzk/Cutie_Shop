package com.superfish.cutieshop.utils;

import android.graphics.Bitmap;

public interface BlurListener {
	void onFinish(Bitmap bit);
}

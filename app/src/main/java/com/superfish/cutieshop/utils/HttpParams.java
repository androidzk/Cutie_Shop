package com.superfish.cutieshop.utils;

import android.text.TextUtils;

import com.superfish.cutieshop.BaseApp;
import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.http.ZokeParams;
import com.tencent.bugly.crashreport.common.strategy.UserInfoBean;

import org.apache.http.NameValuePair;

import java.io.File;
import java.util.List;

/**
 * @author Jack
 */
public class HttpParams {
    //设置通用参数
    public static ZokeParams getDefault(String url, int hasCode) {
        ZokeParams params = new ZokeParams(hasCode, url);
        params.addBodyParameter("os", "1");
        return params;
    }

    public static ZokeParams getDefaultByGet(String url, int hasCode) {
        ZokeParams params = new ZokeParams(hasCode, url);
        params.addQueryStringParameter("os", "1");
        return params;
    }


    /**
     * 获取设备登陆的参数
     **/
    public static ZokeParams getLoginDevice(int hasCode) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.login_device, hasCode);
        params.addBodyParameter("channel", BaseApp.getAppChannel());
        params.addBodyParameter("deviceid", BaseApp.getDeviceId());
        params.addBodyParameter("clienttime", String.valueOf(System.currentTimeMillis() / 1000));
        return params;
    }


    /**
     * 获取编辑个人资料的参数
     *
     * @param hasCode
     * @param gender   1男 2女 0是不修改
     * @param avatar   空代表不修改
     * @param nikename 空代表不修改
     * @return
     */
    public static ZokeParams getEdit(int hasCode, int gender, String avatar, String nikename) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.edit, hasCode);
        LogTest.wlj("avaer=" + avatar + ";Url=" + BaseConfig.UrlBank.edit);
        if (gender != 0)
            params.addBodyParameter("gender", gender + "");
        if (!TextUtils.isEmpty(avatar))
            params.addBodyParameter("avatar", new File(avatar));
        if (!TextUtils.isEmpty(nikename))
            params.addBodyParameter("nikename", nikename);
        return params;
    }

    /**
     * 获取主页列表参数 get的获取形式 需要这样设置
     **/
    public static ZokeParams getMainList(int hasCode, int count, int page) {
        ZokeParams params = getDefaultByGet(BaseConfig.UrlBank.mainList, hasCode);
        params.addQueryStringParameter("count", "" + count);
        params.addQueryStringParameter("page", "" + page);
        return params;
    }

    /**
     * 获取主页列表参数 get的获取形式 需要这样设置 传时间 精确到秒
     **/
    public static ZokeParams getMainListV1(int hasCode, int count, int page, String time) {
        ZokeParams params = getDefaultByGet(BaseConfig.UrlBank.mainList_v1, hasCode);
        params.addQueryStringParameter("count", "" + count);
        params.addQueryStringParameter("page", "" + page);
        if (!TextUtils.isEmpty(time)) {
            params.addQueryStringParameter("timelimit", "" + time);
        }
        return params;
    }

    /**
     * 获取稿件
     *
     * @param hasCode
     * @param tid
     * @return
     */
    public static ZokeParams getPhaseDetail(int hasCode, String tid) {
        ZokeParams params = getDefaultByGet(BaseConfig.UrlBank.phaseDetail, hasCode);
        params.addQueryStringParameter("tid", tid);
        return params;
    }


    /**
     * 获取弹幕列表
     *
     * @param hasCode
     * @param page
     * @param tid
     * @return
     */
    public static ZokeParams getBarrages(int hasCode, int page, int tid) {
        ZokeParams params = getDefaultByGet(BaseConfig.UrlBank.barrages, hasCode);
        params.addQueryStringParameter("count", "99");
        params.addQueryStringParameter("page", "" + page);
        params.addQueryStringParameter("tid", "" + tid);
        return params;
    }


    /**
     * 添加弹幕
     *
     * @param hasCode
     * @param tid
     * @param phrase_id
     * @param text
     * @return
     */
    public static ZokeParams addBarrage(int hasCode, int tid, int phrase_id, String text) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.addBarrage, hasCode);
        params.addBodyParameter("tid", "" + tid);
        if (phrase_id != 0)
            params.addBodyParameter("phrase_id", "" + phrase_id);
        params.addBodyParameter("text", "" + text);
        return params;
    }


    /**
     * 统计分享
     *
     * @param hasCode
     * @param tid
     * @param share
     * @return
     */
    public static ZokeParams analysShare(int hasCode, int tid, int share) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.analysShare, hasCode);
        params.addBodyParameter("share", "" + share);
        params.addBodyParameter("tid", "" + tid);
        return params;
    }

    /**
     * 统计商品的对外分享
     */
    public static ZokeParams itemShare(int hasCode, int share, int iid) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.itemShare, hasCode);
        params.addBodyParameter("share", "" + share);
        params.addBodyParameter("iid", "" + iid);
        return params;
    }


    /**
     * 打印请求参数
     **/
    public static void logParams(ZokeParams params) {
        if (params == null)
            return;
        List<NameValuePair> list = params.getQueryStringParams();
        if (list == null)
            return;
        for (NameValuePair pair : list) {
            LogTest.wlj(pair.getName() + ":" + pair.getValue());
        }
    }

    /**
     * 手机号进行注册
     */
    public static ZokeParams mobReg(int hasCode, String mobile, String password, int gender, String avatar, String nikename) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.mob_reg, hasCode);
        params.addBodyParameter("deviceid", BaseApp.getDeviceId());
        params.addBodyParameter("clienttime", String.valueOf(System.currentTimeMillis() / 1000));
        params.addBodyParameter("channel", BaseApp.getAppChannel());
        params.addBodyParameter("mobile", mobile);
        try {
            params.addBodyParameter("pwd", UID.encrypt(password));
        } catch (Exception e) {
            e.printStackTrace();
        }
        params.addBodyParameter("gender", "" + gender);
        if (!TextUtils.isEmpty(avatar)) {
            params.addBodyParameter("avatar", new File(avatar));
        }
        params.addBodyParameter("nikename", nikename);
        return params;
    }

    /**
     * 手机号注册的时候判断是不是已经被注册了
     */
    public static ZokeParams checkMobile(int hasCode, String mobile) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.check_mobile, hasCode);
        params.addBodyParameter("deviceid", BaseApp.getDeviceId());
        try {
            params.addBodyParameter("mobile", UID.encrypt(mobile));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    /**
     * 微博 微信 QQ 淘宝 第三方注册
     */
    public static ZokeParams triLogin(int hasCode, String type, String emblem, int gender, String avatar, String nikename) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.tri_login, hasCode);
        params.addBodyParameter("deviceid", BaseApp.getDeviceId());
        params.addBodyParameter("clienttime", String.valueOf(System.currentTimeMillis() / 1000));
        params.addBodyParameter("channel", BaseApp.getAppChannel());
        params.addBodyParameter("type", type);
        params.addBodyParameter("emblem", emblem);
        params.addBodyParameter("gender", "" + gender);
        params.addBodyParameter("avatar", avatar);
        params.addBodyParameter("nikename", nikename);
        return params;
    }

    /**
     * 手机号登录
     */
    public static ZokeParams mobLogin(int hasCode, String mobile, String pwd) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.mob_login, hasCode);
        params.addBodyParameter("deviceid", BaseApp.getDeviceId());
        params.addBodyParameter("clienttime", String.valueOf(System.currentTimeMillis() / 1000));
        params.addBodyParameter("mobile", mobile);
        try {
            params.addBodyParameter("pwd", UID.encrypt(pwd));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    /**
     * 手机号登录
     */
    public static ZokeParams forgetPwd(int hasCode, String mobile, String new_pwd) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.forget_pwd, hasCode);
        params.addBodyParameter("deviceid", BaseApp.getDeviceId());
        try {
            params.addBodyParameter("mobile", UID.encrypt(mobile));
            params.addBodyParameter("new_pwd", UID.encrypt(new_pwd));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    /**
     * 登录用户修改手机号注册时候的设置的密码
     */
    public static ZokeParams editPwd(int hasCode, String new_pwd, String old_pwd) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.edit_pwd, hasCode);
        try {
            params.addBodyParameter("new_pwd", UID.encrypt(new_pwd));
            params.addBodyParameter("old_pwd", UID.encrypt(old_pwd));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }


    /**
     * 获取评论列表
     *
     * @param hashCode
     * @param iid
     * @param page
     * @return
     */
    public static ZokeParams getComments(int hashCode, int iid, int page) {
        ZokeParams params = getDefaultByGet(BaseConfig.UrlBank.comments, hashCode);
        params.addQueryStringParameter("iid", "" + iid);
        params.addQueryStringParameter("page", "" + page);
        params.addQueryStringParameter("count", "" + 20);//默认30个
        return params;
    }


    /**
     * 添加评论
     **/
    public static ZokeParams getAddComments(int hashCode, int iid, String text) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.addComment, hashCode);
        params.addBodyParameter("iid", iid + "");
        params.addBodyParameter("text", text);
        return params;
    }
    //暂时不加删除评论


    /**
     * 获取心愿清单列表
     **/
    public static ZokeParams getDetails(int hashCode, int page) {
        ZokeParams params = getDefaultByGet(BaseConfig.UrlBank.details, hashCode);
        params.addQueryStringParameter("page", "" + page);
        params.addQueryStringParameter("count", "" + 20);//默认30个
        return params;
    }


    /**
     * 添加或者取消收藏
     **/
    public static ZokeParams addDetail(int hashCode, int detail, int iid) {
        ZokeParams params = getDefault(BaseConfig.UrlBank.addDetail, hashCode);
        params.addBodyParameter("iid", iid + "");
        if (detail == 0) {
            params.addBodyParameter("add", "1");
        }
        return params;
    }

}

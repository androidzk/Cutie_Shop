package com.superfish.cutieshop.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;

import com.superfish.cutieshop.BaseConfig;
import com.superfish.cutieshop.R;
import com.superfish.cutieshop.activities.MainActivity;
import com.superfish.cutieshop.been.Notify;
import com.superfish.cutieshop.parser.GsonNotify;

/**
 * Created by lmh on 15/9/16.
 */
public class NotificationUtils {

    public static void notifyAndOpenContribution(Context mContext, Notify n) {
        if (!PersistTool.getBoolean(BaseConfig.ShareKey.NOTIFY_SWITCH, false)) {
            Intent intent = new Intent();
            intent.setClass(mContext, MainActivity.class);
            intent.putExtra("notify", n);
            NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            // 创建一个PendingIntent，和Intent类似，不同的是由于不是马上调用，需要在下拉状态条出发的activity，所以采用的是PendingIntent,即点击Notification跳转启动到哪个Activity
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0,
                    intent, 0);
            // 下面需兼容Android 2.x版本是的处理方式
            // Notification notify1 = new Notification(R.drawable.message,
            // "TickerText:" + "您有新短消息，请注意查收！", System.currentTimeMillis());
            Notification notify = new Notification();
            notify.icon = R.mipmap.logo;
            notify.tickerText = "购萌早报每日一购";
            notify.when = System.currentTimeMillis();
            notify.setLatestEventInfo(mContext, BaseConfig.APP_NAME,
                    n.content, pendingIntent);
            notify.number = 1;
            notify.flags |= Notification.FLAG_AUTO_CANCEL; // FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
            // 通过通知管理器来发起通知。如果id不同，则每click，在statu那里增加一个提示
            notify.defaults = Notification.DEFAULT_SOUND;
            manager.notify(BaseConfig.NOTIFICATION_ID_CONTRIBUTION, notify);
        }
    }

    public static void notifyAndOpenWebPage(Context mContext, Notify n) {
        if (!PersistTool.getBoolean(BaseConfig.ShareKey.NOTIFY_SWITCH, false)) {
            Intent intent = new Intent();
            intent.setClass(mContext, MainActivity.class);
            intent.putExtra("notify", n);
            NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            // 创建一个PendingIntent，和Intent类似，不同的是由于不是马上调用，需要在下拉状态条出发的activity，所以采用的是PendingIntent,即点击Notification跳转启动到哪个Activity
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0,
                    intent, 0);
            // 下面需兼容Android 2.x版本是的处理方式
            // Notification notify1 = new Notification(R.drawable.message,
            // "TickerText:" + "您有新短消息，请注意查收！", System.currentTimeMillis());
            Notification notify = new Notification();
            notify.icon = R.mipmap.logo;
            notify.tickerText = "购萌早报每日一购";
            notify.when = System.currentTimeMillis();
            notify.setLatestEventInfo(mContext, BaseConfig.APP_NAME,
                    n.content, pendingIntent);
            notify.number = 1;
            notify.flags |= Notification.FLAG_AUTO_CANCEL; // FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
            // 通过通知管理器来发起通知。如果id不同，则每click，在statu那里增加一个提示
            notify.defaults = Notification.DEFAULT_SOUND;
            manager.notify(BaseConfig.NOTIFICATION_ID_WEBPAGE, notify);
        }
    }

    public static void notifyAndOpenPhaseDetail(Context context, GsonNotify gsonNotify) {
        Intent intent = new Intent();
        intent.setClass(context, MainActivity.class);
        intent.putExtra(BaseConfig.INTENT_SHOPITEM, gsonNotify.send_info);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // 创建一个PendingIntent，和Intent类似，不同的是由于不是马上调用，需要在下拉状态条出发的activity，所以采用的是PendingIntent,即点击Notification跳转启动到哪个Activity
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        // 下面需兼容Android 2.x版本是的处理方式
        // Notification notify1 = new Notification(R.drawable.message,
        // "TickerText:" + "您有新短消息，请注意查收！", System.currentTimeMillis());
        Notification notify = new Notification();
        notify.icon = R.mipmap.logo;
        notify.tickerText = "购萌早报每日一购";
        notify.when = System.currentTimeMillis();
        notify.setLatestEventInfo(context, BaseConfig.APP_NAME,
                gsonNotify.send_contents, pendingIntent);
//        notify.number = 4;
        notify.flags |= Notification.FLAG_AUTO_CANCEL; // FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
        // 通过通知管理器来发起通知。如果id不同，则每click，在statu那里增加一个提示
        if (!CommonUtil.isShowRed(context)) {
            notify.defaults = Notification.DEFAULT_ALL;
            LogTest.lmh("进入if");
//            notify.sound = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, "5");
        } else {
            LogTest.lmh("进入else");
            notify.sound = null;
        }
        manager.notify(BaseConfig.NOTIFICATION_ID_CONTRIBUTION, notify);


    }
}

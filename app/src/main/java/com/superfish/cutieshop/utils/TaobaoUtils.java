package com.superfish.cutieshop.utils;

import android.app.Activity;

import com.alibaba.sdk.android.AlibabaSDK;
import com.alibaba.sdk.android.ResultCode;
import com.alibaba.sdk.android.login.LoginService;
import com.alibaba.sdk.android.login.callback.LoginCallback;
import com.alibaba.sdk.android.session.model.Session;
import com.alibaba.sdk.android.trade.ItemService;
import com.alibaba.sdk.android.trade.callback.TradeProcessCallback;
import com.alibaba.sdk.android.trade.model.TaokeParams;
import com.alibaba.sdk.android.trade.model.TradeResult;
import com.superfish.cutieshop.BaseConfig;
import com.taobao.tae.sdk.webview.TaeWebViewUiSettings;

/**
 * Created by lmh on 15/9/15.
 */
public class TaobaoUtils {
    public final static int TAOBAO = 1;
    public final static int TIANMAO = 2;
    public void showLogin(final Activity activity) {
        LoginService loginService = AlibabaSDK.getService(LoginService.class);
        loginService.showLogin(activity, new LoginCallback() {

            @Override
            public void onSuccess(Session session) {
                CutieToast.show(activity, "欢迎" + session.getUser().nick + session.getUser().avatarUrl);
            }

            @Override
            public void onFailure(int code, String message) {
                CutieToast.show(activity, "授权取消" + code + message);
            }
        });
    }

    public static void showTaokeItemDetail(final Activity activity, int type, String openId) {
        TaeWebViewUiSettings taeWebViewUiSettings = new TaeWebViewUiSettings();
        TaokeParams taokeParams = new TaokeParams();
        taokeParams.pid = BaseConfig.TAOBAOKE_PID;
        taokeParams.unionId = "null";
        ItemService itemService = AlibabaSDK.getService(ItemService.class);
        itemService.showTaokeItemDetailByOpenItemId(activity, new TradeProcessCallback() {

            @Override
            public void onPaySuccess(TradeResult tradeResult) {
                CutieToast.show(activity, "支付成功");
            }

            @Override
            public void onFailure(int code, String msg) {
                if (code == ResultCode.QUERY_ORDER_RESULT_EXCEPTION.code) {
                    CutieToast.show(activity, "确认交易订单失败");
                } else {
                    CutieToast.show(activity, "交易取消");
                }
            }
        }, taeWebViewUiSettings, openId, type, null, taokeParams);
    }
}

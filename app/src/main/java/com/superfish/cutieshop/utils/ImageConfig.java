package com.superfish.cutieshop.utils;

import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DefaultConfigurationFactory;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.superfish.cutieshop.R;

/**
 * @author JackWu  获取ImageLoader的加载图片的配置信息
 */
public class ImageConfig {


    private static DisplayImageOptions mainOptions = null;

    /**
     * 获取主页的接在图设置
     **/
    public static DisplayImageOptions getMainOptions() {
        if (mainOptions != null)
            return mainOptions;
        mainOptions = new DisplayImageOptions.Builder()//
                // 空url的失败
                .showImageForEmptyUri(R.drawable.image_failed)
                        // 下载或解码失败
                .showImageOnFail(R.drawable.image_failed)
                        // 下载过程中
                .showImageOnLoading(R.drawable.image_loading)
                        // 设置下载的图片是否缓存在内存中
                .cacheInMemory(true)
                        // 设置下载的图片是否缓存在SD卡中
                .cacheOnDisk(true)
                        // 保留Exif信息
                .considerExifParams(true)
                        // 设置图片以如何的编码方式显示
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                        // 设置图片的解码类型
                .bitmapConfig(Bitmap.Config.RGB_565)
                        // 设置图片在下载前是否重置，复位
                .resetViewBeforeLoading(true)
                        // 设置图片下载前的延迟
                .delayBeforeLoading(100)// int
                .displayer(DefaultConfigurationFactory.createBitmapDisplayer())// 圆角
                        //
                .build();
        return mainOptions;
    }


    private static DisplayImageOptions photos = null;

    /**
     * 相册 不做
     **/
    public static DisplayImageOptions getPhotoOptions() {
        if (photos != null)
            return photos;
        photos = new DisplayImageOptions.Builder()//
                // 空url的失败
                .showImageForEmptyUri(R.drawable.head_failed)
                        // 下载或解码失败
                .showImageOnFail(R.drawable.head_failed)
                        // 下载过程中
                .showImageOnLoading(R.drawable.head_loading)
                .cacheInMemory(true)
                .cacheOnDisk(false)
                        // 保留Exif信息
                .considerExifParams(true)
                        // 设置图片以如何的编码方式显示
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                        // 设置图片的解码类型
                .bitmapConfig(Bitmap.Config.RGB_565)
                        // 设置图片在下载前是否重置，复位
                .resetViewBeforeLoading(true)
                        // 设置图片下载前的延迟
                .delayBeforeLoading(100)// int
                .displayer(DefaultConfigurationFactory.createBitmapDisplayer())// 圆角
                        //
                .build();
        return photos;
    }


    private static DisplayImageOptions mainOptions2 = null;

    /**
     * 获取主页的接在图设置
     **/
    public static DisplayImageOptions getMainOptions2() {
        if (mainOptions2 != null)
            return mainOptions2;
        mainOptions2 = new DisplayImageOptions.Builder()//
                // 空url的失败
                .showImageForEmptyUri(R.drawable.image_failed)
                        // 下载或解码失败
                .showImageOnFail(R.drawable.image_failed)
                        // 下载过程中
                .showImageOnLoading(R.drawable.image_loading)
                        // 设置下载的图片是否缓存在内存中
                .cacheInMemory(true)
                        // 设置下载的图片是否缓存在SD卡中
                .cacheOnDisk(true)
                        // 保留Exif信息
                .considerExifParams(true)
                        // 设置图片以如何的编码方式显示
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                        // 设置图片的解码类型
                .bitmapConfig(Bitmap.Config.RGB_565)
                        // 设置图片在下载前是否重置，复位
                .resetViewBeforeLoading(true)
                        // 设置图片下载前的延迟
                .delayBeforeLoading(100)// int
                .displayer(new RoundedBitmapDisplayer(30))// 圆角
                        //
                .build();
        return mainOptions2;
    }

    private static DisplayImageOptions squreOptions = null;

    /**
     * 圆头像配置
     **/
    public static DisplayImageOptions getSqureOptions() {
        if (squreOptions != null)
            return squreOptions;
        squreOptions = new DisplayImageOptions.Builder()//
                // 空url的失败
                .showImageForEmptyUri(R.drawable.head_failed)
                        // 下载或解码失败
                .showImageOnFail(R.drawable.head_failed)
                        // 下载过程中
                .showImageOnLoading(R.drawable.head_loading)
                        // 设置下载的图片是否缓存在内存中
                .cacheInMemory(true)
                        // 设置下载的图片是否缓存在SD卡中
                .cacheOnDisk(true)
                        // 保留Exif信息
                .considerExifParams(true)
                        // 设置图片以如何的编码方式显示
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                        // 设置图片的解码类型
                .bitmapConfig(Bitmap.Config.RGB_565)
                        // 设置图片在下载前是否重置，复位
                .resetViewBeforeLoading(true)
                        // 设置图片下载前的延迟
                .delayBeforeLoading(100)// int
                .displayer(new RoundedBitmapDisplayer(36))// 圆角
                        //
                .build();
        return squreOptions;
    }


    private static DisplayImageOptions circlHeadOptions = null;

    /**
     * 圆头像配置
     **/
    public static DisplayImageOptions getCirclHeadOptions() {
        if (circlHeadOptions != null)
            return circlHeadOptions;
        circlHeadOptions = new DisplayImageOptions.Builder()//
                // 空url的失败
                .showImageForEmptyUri(R.drawable.default_head)
                        // 下载或解码失败
                .showImageOnFail(R.drawable.default_head)
                        // 下载过程中
                .showImageOnLoading(R.drawable.default_head)
                        // 设置下载的图片是否缓存在内存中
                .cacheInMemory(true)
                        // 设置下载的图片是否缓存在SD卡中
                .cacheOnDisk(true)
                        // 保留Exif信息
                .considerExifParams(true)
                        // 设置图片以如何的编码方式显示
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                        // 设置图片的解码类型
                .bitmapConfig(Bitmap.Config.RGB_565)
                        // 设置图片在下载前是否重置，复位
                .resetViewBeforeLoading(true)
                        // 设置图片下载前的延迟
                .delayBeforeLoading(100)// int
                .displayer(new RoundedBitmapDisplayer(500))// 圆角
                        //
                .build();
        return circlHeadOptions;
    }

}

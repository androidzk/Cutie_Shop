package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.BarrageBeen;
import com.superfish.cutieshop.gson.AbsData;

/**
 * @author Jack
 */
public class GsonAddBarrage extends AbsData<GsonAddBarrage> {

    public BarrageBeen info;

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonAddBarrage> getTypeToken() {
        return new TypeToken<GsonAddBarrage>() {
        };
    }
}

package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.User;
import com.superfish.cutieshop.gson.AbsData;

/**
 * 设备号登陆的解析器
 */
public class GsonLogin extends AbsData<GsonLogin> {
    public LoginInfo info;

    public class LoginInfo {
        public String token;
        public User user_info;
    }

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonLogin> getTypeToken() {
        return new TypeToken<GsonLogin>() {
        };
    }
}

package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.CommentInfo;
import com.superfish.cutieshop.gson.AbsData;

/**
 * 解析评论列表
 */
public class GsonComments extends AbsData<GsonComments> {
    public Info info;

    public class Info {
        public CommentInfo[] list;
        public int next_page;
    }

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonComments> getTypeToken() {
        return new TypeToken<GsonComments>() {
        };
    }
}

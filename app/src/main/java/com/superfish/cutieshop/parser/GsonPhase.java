package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.Phase;
import com.superfish.cutieshop.gson.AbsData;

/**
 * @author Jack
 */
public class GsonPhase extends AbsData<GsonPhase> {

    public Phase[] info;

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonPhase> getTypeToken() {
        return new TypeToken<GsonPhase>() {
        };
    }
}

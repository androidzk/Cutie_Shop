package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.BarrageBeen;
import com.superfish.cutieshop.gson.AbsData;

/**
 * @author JackWu
 */
public class GsonBarrage extends AbsData<GsonBarrage> {

    public Info info;

    public class Info {
        public int next_page;
        public BarrageBeen[] list;
    }

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonBarrage> getTypeToken() {
        return new TypeToken<GsonBarrage>() {
        };
    }
}

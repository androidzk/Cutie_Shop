package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.gson.AbsData;

/**
 * 忘记密码
 */
public class GsonForgetPwd extends AbsData<GsonForgetPwd> {
    public ForgetPwdInfo info;

    public class ForgetPwdInfo {
        public String mobile;
    }

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonForgetPwd> getTypeToken() {
        return new TypeToken<GsonForgetPwd>() {
        };
    }
}

package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.Product;
import com.superfish.cutieshop.gson.AbsData;

/**
 * @author JackWu
 */
public class GsonDetails extends AbsData<GsonDetails> {

    public Info info;

    public class Info {
        public Product[] list;
        public int next_page;
    }

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonDetails> getTypeToken() {
        return new TypeToken<GsonDetails>() {
        };
    }
}

package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.ShopItem;
import com.superfish.cutieshop.gson.AbsData;

/**
 * Created by lmh on 15/9/17.
 */

public class GsonNotify extends AbsData<GsonNotify> {
    public String send_contents;
    public String send_avatar;
    public int send_type;
    public String send_value;
    public long send_time;
    public ShopItem send_info;

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonNotify> getTypeToken() {
        return new TypeToken<GsonNotify>() {
        };
    }
}

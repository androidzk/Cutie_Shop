package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.PhaseDetail;
import com.superfish.cutieshop.gson.AbsData;

/**
 */
public class GsonPhaseDetail extends AbsData<GsonPhaseDetail> {
    public PhaseDetail[] info;

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonPhaseDetail> getTypeToken() {
        return new TypeToken<GsonPhaseDetail>() {
        };
    }
}

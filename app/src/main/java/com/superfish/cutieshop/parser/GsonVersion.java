package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.Version;
import com.superfish.cutieshop.gson.AbsData;

/**
 * @author JackWu
 */
public class GsonVersion extends AbsData<GsonVersion> {


    public Version info;


    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonVersion> getTypeToken() {
        return new TypeToken<GsonVersion>() {
        };
    }
}

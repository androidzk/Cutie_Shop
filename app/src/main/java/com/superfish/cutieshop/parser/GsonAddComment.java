package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.CommentInfo;
import com.superfish.cutieshop.gson.AbsData;

/**
 * 解析评论列表
 */
public class GsonAddComment extends AbsData<GsonAddComment> {
    public CommentInfo info;

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonAddComment> getTypeToken() {
        return new TypeToken<GsonAddComment>() {
        };
    }
}

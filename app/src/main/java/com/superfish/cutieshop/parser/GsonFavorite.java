package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.gson.AbsData;

/**
 * @author JackWu
 */
public class GsonFavorite extends AbsData<GsonFavorite> {
    public AddFavorite info;

    public class AddFavorite {
        public int iid;
        public int status;
        public int counts;
    }

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonFavorite> getTypeToken() {
        return new TypeToken<GsonFavorite>() {
        };
    }
}

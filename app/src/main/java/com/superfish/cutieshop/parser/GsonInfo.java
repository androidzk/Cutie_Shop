package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.ShopItem;
import com.superfish.cutieshop.gson.AbsData;

/**
 * @author Jack
 */
public class GsonInfo extends AbsData<GsonInfo> {

    public Info info;

    public class Info {
        public ShopItem article;
        public int allow_comment;
    }

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonInfo> getTypeToken() {
        return new TypeToken<GsonInfo>() {
        };
    }
}

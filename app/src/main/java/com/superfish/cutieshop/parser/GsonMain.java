package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.been.ShopItem;
import com.superfish.cutieshop.gson.AbsData;

/**
 * @author Jack
 */
public class GsonMain extends AbsData<GsonMain> {

    public Info info;

    public class Info {
        public ShopItem top;
        public ShopItem[] list;
        public int next_page;
    }

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonMain> getTypeToken() {
        return new TypeToken<GsonMain>() {
        };
    }
}

package com.superfish.cutieshop.parser;

import com.google.gson.reflect.TypeToken;
import com.superfish.cutieshop.gson.AbsData;

/**
 * 设备号登陆的解析器
 */
public class GsonCheckMobile extends AbsData<GsonCheckMobile> {
    public CheckMobileInfo info;

    public class CheckMobileInfo {
        public String mobile;
        public String check;
    }

    @Override
    protected String[] typeValues() {
        return null;
    }

    @Override
    protected TypeToken<GsonCheckMobile> getTypeToken() {
        return new TypeToken<GsonCheckMobile>() {
        };
    }
}
